export const SingleProductMock = {
  title: 'Заголовок рецепта',
  nutritionalValue: {
    protein: 10,
    carbohydrates: 15,
    fat: 20,
  },
  pro: true,
  description:
    'Большое и длинное общее описание рецепта для понимания пользователем что он будет готовить',
  time: '1',
  timeType: 'hour',
  cookware: [
    {
      title: "chef's knife",
      count: 3,
    },
    {
      title: 'colander',
      count: 4,
    },
    {
      title: 'cutting board',
      count: 1,
    },
    {
      title: 'grater (optional)',
      count: 3,
    },
  ],
  ingredients: [
    {
      title: 'medium carrot',
      count: 1,
    },
    {
      title: 'cup cashews, roasted unsalted',
      count: 0.5,
    },
    {
      title: 'English cucumber',
      count: 0.5,
    },
    {
      title: 'piece ginger root',
      count: 1,
    },
    {
      title: 'small bunch green onions',
      count: 0.5,
    },
  ],
  instruction: [
    {
      title: 'Fill a large pot halfway with water and bring to a boil.',
      description: '',
    },
    {
      title: 'Wash and dry the fresh produce.',
      description:
        '½ English cucumber\n1 medium carrot\n¼ small head red cabbage\n½ red bell pepper\n½ small bunch green onions (scallions)\n1 (1 inch) piece ginger root',
    },
    {
      title: 'Peel and grate or mince the ginger; transfer to a medium bowl.',
      description: '',
    },
  ],
};
