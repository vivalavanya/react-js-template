import { Box, Button, Menu, Typography } from '@mui/material';
import React, { useEffect, useState } from 'react';
import { useCookies } from 'react-cookie';
import { useSelector } from 'react-redux';
import styles from './InterfaceInstructionItem.module.scss';
export const InterfaceInstructionItem = ({
  step,
  title,
  actionHandler,
  actionText,
}) => {
  const [anchorEl, setAnchorEl] = useState(null);
  const [open, setOpen] = useState(false);
  const instruction = useSelector((state) => state.instruction);
  const [cookie] = useCookies('instruction');
  useEffect(() => {
    if (instruction.step == step) {
      const el = document.getElementById(step);

      setAnchorEl(el);
      setOpen(true);
    } else {
      setOpen(false);
    }
  }, [instruction, cookie]);
  return (
    <Menu
      id="basic-menu"
      anchorEl={anchorEl}
      open={open}
      MenuListProps={{
        'aria-labelledby': 'basic-button',
      }}
      PaperProps={{
        elevation: 0,
        sx: {
          overflow: 'visible',
          filter: 'drop-shadow(0px 2px 8px rgba(0,0,0,0.32))',
          background: '#222',
          maxWidth: '300px',
          mt: 1.5,
          '& .MuiAvatar-root': {
            width: 32,
            height: 32,
            ml: -0.5,
            mr: 1,
          },
          '&::before': {
            content: '""',
            display: 'block',
            position: 'absolute',
            top: 0,
            right: 14,
            width: 10,
            height: 10,
            background: '#222',
            transform: 'translateY(-50%) rotate(45deg)',
            zIndex: 0,
          },
        },
      }}
      transformOrigin={{ horizontal: 'right', vertical: 'top' }}
      anchorOrigin={{ horizontal: 'right', vertical: 'bottom' }}
    >
      <Box sx={{ p: 2, background: '#222' }}>
        <Typography sx={{ color: '#fff', fontWeight: 'bold' }}>
          {title}
        </Typography>
        <Button
          size="small"
          sx={{ mt: 2 }}
          variant="contained"
          color="success"
          onClick={actionHandler}
        >
          {actionText}
        </Button>
      </Box>
    </Menu>
  );
};
