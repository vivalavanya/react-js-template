import React, { useEffect, useState } from 'react';
import {
  Page,
  Text,
  View,
  Document,
  StyleSheet,
  Font,
  Image,
} from '@react-pdf/renderer';
import { add, format } from 'date-fns';

// Create styles
// Register Font
Font.register({
  family: 'Roboto',
  src: 'https://cdnjs.cloudflare.com/ajax/libs/ink/3.1.10/fonts/Roboto/roboto-light-webfont.ttf',
});
Font.register({
  family: 'RobotoBold',
  src: 'https://cdnjs.cloudflare.com/ajax/libs/ink/3.1.10/fonts/Roboto/roboto-bold-webfont.ttf',
});

// Create style with font-family
const s = StyleSheet.create({
  h2: {
    fontWeight: 'bold',
  },
  page: {
    fontFamily: 'Roboto',
    padding: '50px',
    display: 'flex',
    flexDirection: 'column',
    fontSize: '22px',
  },
  flex: {
    display: 'flex',
    // justifyContent: 'space-between',
    marginTop: 'auto',
    width: '100%',
  },
  footerBlock: {
    display: 'block',
    border: 'solid 10px red',
  },
  logo: {
    width: '230px',
  },
  certBG: {
    position: 'absolute',
    top: 0,
    right: 0,
    width: '100%',
    height: '35%',
    // zIndex: -1,
  },
  title: {
    fontWeight: 'bold',
    fontSize: '32px',
    fontFamily: 'RobotoBold',
    marginBottom: '30px',
  },
  expire: {
    marginTop: '50px',
  },
  customText: {
    marginTop: '30px',
  },

  table: {
    display: 'table',
    width: 'auto',
    borderStyle: 'solid',
    borderWidth: 1,
    borderRightWidth: 0,
    borderBottomWidth: 0,
    marginTop: '40px',
  },
  tableRow: {
    margin: 'auto',
    flexDirection: 'row',
  },
  tableCol: {
    width: '20%',
    borderStyle: 'solid',
    borderWidth: 1,
    borderLeftWidth: 0,
    borderTopWidth: 0,
  },
  tableCol2: {
    width: '50%',
    borderStyle: 'solid',
    borderWidth: 1,
    borderLeftWidth: 0,
    borderTopWidth: 0,
  },
  tableCell: {
    margin: 'auto',
    marginTop: 5,
    fontWeight: 'bold',
    marginBottom: 5,
    fontSize: 10,
  },
});

// Create Document Component
export const RecieptPdf = ({ reciept }) => {
  return (
    <Document language="ru">
      <Page size="A4" style={s.page}>
        {/* <Image src="cert-logos.png" style={s.logo} /> */}
        <View style={s.flex}>
          <Text style={s.title}>{reciept?.title}</Text>
          <Text as="p" size="m">
            {reciept?.description}
          </Text>
        </View>
        <View style={s.flex}>
          <Text as="h2" style={s.h2}>
            Приборы
          </Text>
          {reciept?.cookware?.map((c) => (
            <Text as="p">{c.count + 'x - ' + c.title}</Text>
          ))}
        </View>
        <View style={s.flex}>
          <Text as="h2" style={s.h2}>
            Ингредиенты
          </Text>
          {reciept?.ingredients?.map((c) => (
            <Text as="p">{c.count + 'x - ' + c.title}</Text>
          ))}
        </View>
        <View style={s.flex}>
          <Text as="h2" style={s.h2}>
            Инструкция
          </Text>

          {reciept?.ingredients?.map((c) => (
            <>
              <Text as="p">{c.title}</Text>
              <Text as="p" size="m">
                {c.description}
              </Text>
            </>
          ))}
        </View>
        <View style={s.flex}>
          <Text style={s.footerBlock}></Text>
        </View>
      </Page>
    </Document>
  );
};
