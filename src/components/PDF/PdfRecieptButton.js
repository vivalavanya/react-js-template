import { IconButton } from '@mui/material';
import React from 'react';
import LocalPrintshopOutlinedIcon from '@mui/icons-material/LocalPrintshopOutlined';
import { RecieptPdf } from './RecieptPdf';
import { PDFDownloadLink } from '@react-pdf/renderer';

export const PdfRecieptButton = ({ reciept }) => {
  return (
    <PDFDownloadLink
      document={<RecieptPdf reciept={reciept} />}
      fileName={'Рецепт'}
    >
      {({ blob, url, loading, error }) =>
        reciept && (
          <IconButton
            color="inherit"
            aria-label="Remove product from day"
            sx={{ fontSize: '8px', opacity: 0.4 }}
          >
            <LocalPrintshopOutlinedIcon />
          </IconButton>
        )
      }
    </PDFDownloadLink>
  );
};
