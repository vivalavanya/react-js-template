import {
  Box,
  Button,
  Chip,
  IconButton,
  Stack,
  Tab,
  Tabs,
  TextField,
  Typography,
} from '@mui/material';
import React, { useState } from 'react';
import styles from './ProductItem.module.scss';
import SubjectOutlinedIcon from '@mui/icons-material/SubjectOutlined';
import LocalDiningOutlinedIcon from '@mui/icons-material/LocalDiningOutlined';
import CategoryOutlinedIcon from '@mui/icons-material/CategoryOutlined';
import ChecklistOutlinedIcon from '@mui/icons-material/ChecklistOutlined';
import FavoriteBorderOutlinedIcon from '@mui/icons-material/FavoriteBorderOutlined';
import { PdfRecieptButton } from '../PDF/PdfRecieptButton';

export const RecieptFullData = ({
  reciept,
  addToMenuHandler,
  removeToMenuHandler,
  hiddenButtons = false,
  fullmenu,
  currentDay,
  showCounts = true,
}) => {
  const [value, setValue] = React.useState(0);
  const [showRemoveItem, setShowRemoveItem] = useState(false);

  useState(() => {
    let result = false;
    if (fullmenu?.by_week_days) {
      result = fullmenu?.structured_menu[currentDay]?.some(
        (item) => item._id == reciept._id,
      );
    } else {
      result = fullmenu?.unstructured_menu?.some(
        (item) => item._id == reciept._id,
      );
    }

    setShowRemoveItem(result);
  }, []);
  return (
    <Stack direction="row" sx={{ height: '100vh', overflow: 'hidden' }}>
      <Box
        sx={{
          background: '#f9f9f9',
          height: '100%',
          py: 2,
          px: 0,
          minWidth: '280px',
        }}
      >
        <Stack
          justifyContent="space-between"
          sx={{ height: '100vh', width: '400px' }}
        >
          <Box>
            <Typography as="p" variant="h6" sx={{ mb: 2, px: 1.5 }}>
              {reciept?.title}{' '}
              <Chip
                label={reciept?.complexity}
                size="small"
                color="warning"
                sx={{ ml: 1 }}
              />
            </Typography>

            <ul className={styles.RecieptNavigation}>
              <li
                onClick={() => setValue(0)}
                className={value == 0 && styles.activeTab}
              >
                <Box>
                  <Stack direction="row" spacing={2} alignItems="center">
                    <SubjectOutlinedIcon />
                    <Typography>Общее описание</Typography>
                  </Stack>
                </Box>
              </li>
              <li
                onClick={() => setValue(1)}
                className={value === 1 && styles.activeTab}
              >
                <Box>
                  <Stack direction="row" spacing={2}>
                    <LocalDiningOutlinedIcon />
                    <Typography>Приборы</Typography>
                  </Stack>
                </Box>
              </li>
              <li
                onClick={() => setValue(2)}
                className={value === 2 && styles.activeTab}
              >
                <Box>
                  <Stack direction="row" spacing={2}>
                    <CategoryOutlinedIcon />
                    <Typography>Ингредиенты</Typography>
                  </Stack>
                </Box>
              </li>
              <li
                onClick={() => setValue(3)}
                className={value === 3 && styles.activeTab}
              >
                <Box>
                  <Stack direction="row" spacing={2}>
                    <ChecklistOutlinedIcon />
                    <Typography>Инструкция</Typography>
                  </Stack>
                </Box>
              </li>
            </ul>
          </Box>
          <Box sx={{ p: 2, mt: 'auto', mb: 2 }}>
            <Stack direction="row" spacing={1}>
              {!hiddenButtons &&
                (showRemoveItem ? (
                  <Button
                    color="error"
                    variant="contained"
                    fullWidth
                    onClick={() => removeToMenuHandler(reciept)}
                  >
                    Убрать
                  </Button>
                ) : (
                  <Button
                    color="success"
                    variant="contained"
                    fullWidth
                    onClick={() => addToMenuHandler(reciept)}
                  >
                    Добавить
                  </Button>
                ))}

              {showCounts && (
                <Stack direction="row" spacing={1} alignItems="center">
                  <IconButton size="small">-</IconButton>
                  <TextField sx={{ width: '50px' }} size="small" value={0} />
                  <IconButton size="small">+</IconButton>
                </Stack>
              )}
              <Box>
                <PdfRecieptButton reciept={reciept} />
              </Box>
              <Box>
                <IconButton
                  color="inherit"
                  aria-label="Remove product from day"
                  sx={{ fontSize: '8px', opacity: 0.4, mr: 1 }}
                >
                  <FavoriteBorderOutlinedIcon />
                </IconButton>
              </Box>
            </Stack>
          </Box>
        </Stack>
      </Box>
      <Box
        sx={{
          height: '100vh',
          overflow: 'scroll',
          width: '900px',
          maxWidth: '100%',
        }}
      >
        {value === 0 && (
          <Box sx={{ p: 3 }}>
            <Typography as="h3" variant="h6">
              Общее описание
            </Typography>
            <Typography>{reciept?.description}</Typography>

            <Stack direction="row" spacing={2} sx={{ mt: 2 }}>
              <Chip
                label={`Общее время приготавления: ${reciept?.durationTime?.time}${reciept?.durationTime?.timeType}.`}
              />
              <Chip
                label={`Активное время приготавления: ${reciept?.activeDurationTime?.time}${reciept?.activeDurationTime?.timeType}.`}
              />
            </Stack>
          </Box>
        )}
        {value === 1 && (
          <Box sx={{ p: 3 }}>
            <Typography as="h3" variant="h6">
              Вам понадобится
            </Typography>
            {reciept?.cookware?.map((item) => (
              <Stack sx={{ py: 2, borderBottom: 'solid 1px #eee' }}>
                <Stack
                  direction="row"
                  spacing={2}
                  justifyContent="space-between"
                >
                  <Box>
                    <Typography> {item}</Typography>
                  </Box>
                </Stack>
              </Stack>
            ))}
          </Box>
        )}
        {value === 2 && (
          <Box sx={{ p: 3 }}>
            <Typography as="h3" variant="h6">
              Ингредиенты
            </Typography>
            {reciept?.ingredients?.map((item) => (
              <Stack sx={{ py: 2, borderBottom: 'solid 1px #eee' }}>
                <Stack
                  direction="row"
                  spacing={2}
                  justifyContent="space-between"
                >
                  <Box>
                    <Typography> {item?.label?.title}</Typography>
                  </Box>
                  <Box>
                    <Typography>
                      {item?.count} {item?.measure}
                    </Typography>
                  </Box>
                </Stack>
              </Stack>
            ))}
          </Box>
        )}
        {value === 3 && (
          <Box sx={{ p: 3 }}>
            <Typography as="h3" variant="h6">
              Инструкция
            </Typography>
            {reciept?.instruction?.map((item, index) => (
              <Stack key={index} sx={{ py: 2, borderBottom: 'solid 1px #eee' }}>
                <Stack direction="row" spacing={2}>
                  <Box>
                    <Typography
                      as="p"
                      variant="h5"
                      sx={{ opacity: 0.3, fontWeight: 'bold' }}
                    >
                      {index + 1}
                    </Typography>
                  </Box>
                  <Box>
                    <Typography sx={{ fontWeight: 'bold' }}>
                      {item?.title}
                    </Typography>
                    {!!item?.ingredients?.length && (
                      <>
                        {' '}
                        <Typography sx={{ mt: 2, fontWeight: 'bold' }}>
                          Ингредиенты:
                        </Typography>
                        {item?.ingredients?.map((i, index) => (
                          <Typography>
                            <span
                              style={{
                                fontWeight: 'bold',
                                color: '#818181',
                                margin: '0 10px',
                              }}
                            >
                              {index + 1}
                            </span>{' '}
                            {i.count} {i.measure}
                          </Typography>
                        ))}
                      </>
                    )}
                    {/* "prop": 0.25,
                            "count": 1.0,
                            "measure": "ст. ложка" */}
                  </Box>
                </Stack>
              </Stack>
            ))}
          </Box>
        )}
      </Box>
    </Stack>
  );
};
