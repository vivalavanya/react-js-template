import {
  Box,
  Button,
  Card,
  CardContent,
  Chip,
  IconButton,
  Stack,
  TextField,
  Typography,
} from '@mui/material';
import React from 'react';
import styles from './ProductItem.module.scss';
export const ProductItem = ({ product, onNewItem, onClickToProduct }) => {
  return (
    <Card
      sx={{
        minWidth: '350px',
        mb: 1,
        boxShadow: 'none',
        borderBottom: 'solid 1px #eee',
        borderRadius: '0',
      }}
    >
      <CardContent sx={{ p: 2, pb: 2 }}>
        <Stack direction="row" spacing={2}>
          <Box
            className={styles.ProductItem__Img}
            onClick={() => onClickToProduct(product)}
          >
            <Chip
              label="pro"
              sx={{
                position: 'absolute',
                top: '5px',
                left: '5px',
                fontSize: '10px',
                background: '#adff2f',
                fontWeight: 'bold',
                px: 0.3,
                py: 0,
              }}
              size="small"
            />
            <img alt="" src={product?.url} />
          </Box>
          <Box>
            <Typography
              as="p"
              sx={{ fontWeight: 'bold', cursor: 'pointer' }}
              onClick={() => onClickToProduct(product)}
            >
              {product?.title}
            </Typography>
            <Stack
              direction="row"
              spacing={2}
              sx={{ mb: 2 }}
              alignItems="center"
            >
              <Chip label="1ч." size="small" />
              <Typography as="p" variant="caption">
                Ккал: {product?.nutritionalValue?.calories}, Б:{' '}
                {product?.nutritionalValue?.proteins}, Ж:{' '}
                {product?.nutritionalValue?.fats}, У:{' '}
                {product?.nutritionalValue?.carbohydrates}
              </Typography>
            </Stack>
            <Stack direction="row" spacing={2}>
              <Button
                variant="outlined"
                color="success"
                size="small"
                onClick={() => onNewItem(product)}
              >
                Добавить
              </Button>
              <Stack direction="row" spacing={1} alignItems="center">
                <IconButton size="small">-</IconButton>
                <TextField sx={{ width: '50px' }} size="small" value={0} />
                <IconButton size="small">+</IconButton>
              </Stack>
            </Stack>
          </Box>
        </Stack>
      </CardContent>
    </Card>
  );
};
