import {
  Box,
  Button,
  Card,
  CardContent,
  Chip,
  Fab,
  IconButton,
  Stack,
  TextField,
  Typography,
} from '@mui/material';
import React, { useEffect, useState } from 'react';
import styles from './ProductItem.module.scss';
import FavoriteBorderOutlinedIcon from '@mui/icons-material/FavoriteBorderOutlined';
import { useApi } from '../../hooks/useApi';
import { DownvoteeRecipe, UpvoteeRecipe } from '../../api/recipe.api';
import { UpadteMenu } from '../../api/menu.api';
export const RecieptBigItem = ({
  reciept,
  onClickToReciept,
  showCounts = true,
  addToMenuHandler,
  onRemoveItem,
  num_serv = 1,
  updateFromMenu = false,
  fullMenu = {},
  currentDay,
  reloadMenuhandler,
}) => {
  const UpvoteeRecipeApi = useApi(UpvoteeRecipe);
  const DownvoteeRecipeApi = useApi(DownvoteeRecipe);
  const [numServ, setNumServ] = useState(num_serv);
  const [showRemoveItem, setShowRemoveItem] = useState(false);
  const [isFavorite, setIsFavorite] = useState(false);
  const UpvoteRecipeHandler = async () => {
    await UpvoteeRecipeApi.sendRequest({ id: reciept?.recipe?._id });
  };
  const DownoteRecipeHandler = async () => {
    await DownvoteeRecipeApi.sendRequest({ id: reciept?.recipe?._id });
  };

  const UpdateMenuApi = useApi(UpadteMenu);
  const UpdateMenuHandler = async (data) => {
    if (updateFromMenu) {
      await UpdateMenuApi.sendRequest(data, fullMenu?._id);

      await reloadMenuhandler();
    }
  };

  useEffect(() => {
    let result = false;
    if (fullMenu?.by_week_days) {
      result = fullMenu?.structured_menu[currentDay]?.some(
        (item) => item._id == reciept?.recipe?._id,
      );
    } else {
      result = fullMenu?.unstructured_menu?.some(
        (item) => item._id == reciept?.recipe?._id,
      );
    }
    setShowRemoveItem(result);
    setIsFavorite(reciept?.recipe?.isFavorite);
  }, []);
  return (
    <Card
      sx={{
        boxShadow: 'none',
        borderRadius: '0',
        width: '100%',
      }}
    >
      <CardContent sx={{ p: 0 }}>
        <Box className={styles.RecieptItemBig__Img}>
          <Stack
            direction="row"
            spacing={1}
            sx={{ position: 'absolute', top: '5px', left: '5px' }}
          >
            {!!reciept?.recipe?.pro && (
              <Chip
                label="pro"
                sx={{
                  fontSize: '10px',
                  background: '#adff2f',
                  fontWeight: 'bold',
                  px: 0.3,
                  py: 0,
                }}
                size="small"
              />
            )}
            <Chip
              label={reciept?.recipe?.complexity}
              sx={{
                fontSize: '10px',
                fontWeight: 'bold',
                px: 0.3,
                py: 0,
              }}
              color="warning"
              size="small"
            />
          </Stack>
          <Fab
            size="small"
            sx={{
              position: 'absolute',
              top: '5px',
              right: '5px',

              width: '35px',
              maxHeight: '5px !important',
            }}
            color={isFavorite ? 'error' : 'inherit'}
            onClick={() => {
              if (!isFavorite) {
                UpvoteRecipeHandler();
              } else {
                DownoteRecipeHandler();
              }
              setIsFavorite(!isFavorite);
            }}
          >
            <FavoriteBorderOutlinedIcon sx={{ fontSize: '15px' }} />
          </Fab>
          <img
            alt=""
            src={reciept?.recipe?.url}
            onClick={() => onClickToReciept(reciept?.recipe)}
          />
        </Box>
        <Typography
          as="p"
          sx={{ fontWeight: 'bold', cursor: 'pointer', lineHeight: 1.1, my: 1 }}
          onClick={() => onClickToReciept(reciept?.recipe)}
        >
          {reciept?.recipe?.title}
        </Typography>
        <Typography sx={{ mb: 2, lineHeight: 1, opacity: 0.6 }} variant="body2">
          {reciept?.recipe?.description?.slice(0, 50) + '...'}
        </Typography>
        <Stack direction="row" spacing={2} sx={{ mb: 2 }} alignItems="center">
          <Chip
            label={
              reciept?.recipe?.activeDurationTime?.time +
              ' ' +
              reciept?.recipe?.activeDurationTime?.timeType +
              '.'
            }
            size="small"
          />
          <Typography as="p" variant="caption">
            Ккал: {reciept?.recipe?.nutritionalValue?.calories}, Б:{' '}
            {reciept?.recipe?.nutritionalValue?.proteins}, Ж:{' '}
            {reciept?.recipe?.nutritionalValue?.fats}, У:{' '}
            {reciept?.recipe?.nutritionalValue?.carbohydrates}
          </Typography>
        </Stack>
        <Stack direction="row" spacing={2}>
          {showRemoveItem ? (
            <Button
              variant="outlined"
              color="error"
              size="small"
              onClick={() => onRemoveItem(reciept?.recipe)}
            >
              Убрать
            </Button>
          ) : (
            <Button
              variant="outlined"
              color="success"
              size="small"
              onClick={() => addToMenuHandler({ ...reciept?.recipe, num_serv })}
            >
              Добавить
            </Button>
          )}

          {showCounts && (
            <Stack direction="row" spacing={1} alignItems="center">
              <IconButton
                size="small"
                onClick={() => {
                  if (updateFromMenu) {
                    if (fullMenu.by_week_days) {
                      UpdateMenuHandler({
                        structured_menu: {
                          ...fullMenu?.structured_menu,
                          [currentDay]: fullMenu?.structured_menu[
                            currentDay
                          ]?.map((r) => {
                            if (r._id == reciept?.recipe?._id) {
                              r.num_serv =
                                r.num_serv - 1 >= 1 ? r.num_serv - 1 : 1;
                            }
                            return r;
                          }),
                        },
                      });
                    } else {
                      UpdateMenuHandler({
                        unstructured_menu: [
                          ...fullMenu?.unstructured_menu?.map((r) => {
                            if (r._id == reciept?.recipe?._id) {
                              r.num_serv =
                                r.num_serv - 1 >= 1 ? r.num_serv - 1 : 1;
                            }
                            return r;
                          }),
                        ],
                      });
                    }
                  }
                  setNumServ(numServ - 1 >= 1 ? numServ - 1 : 1);
                }}
              >
                -
              </IconButton>
              <TextField
                sx={{ width: '50px' }}
                size="small"
                value={numServ}
                type="number"
                onChange={({ target: { value } }) => setNumServ(+value)}
              />
              <IconButton
                size="small"
                onClick={() => {
                  if (updateFromMenu) {
                    if (fullMenu.by_week_days) {
                      UpdateMenuHandler({
                        structured_menu: {
                          ...fullMenu?.structured_menu,
                          [currentDay]: fullMenu?.structured_menu[
                            currentDay
                          ]?.map((r) => {
                            if (r._id == reciept?.recipe?._id) {
                              r.num_serv = +r.num_serv + 1;
                            }
                            return r;
                          }),
                        },
                      });
                    } else {
                      UpdateMenuHandler({
                        unstructured_menu: [
                          ...fullMenu?.unstructured_menu?.map((r) => {
                            if (r._id == reciept?.recipe?._id) {
                              r.num_serv = +r.num_serv + 1;
                              console.log({
                                r: r._id,
                                reciept: reciept?.recipe?._id,
                                num_serv: +r.num_serv + 1,
                              });
                            }
                            return r;
                          }),
                        ],
                      });
                    }
                  }
                  setNumServ(+numServ + 1);
                }}
              >
                +
              </IconButton>
            </Stack>
          )}
        </Stack>
      </CardContent>
    </Card>
  );
};
