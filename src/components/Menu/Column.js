import React from 'react';
import { useDrop } from 'react-dnd';
import { MenuProductItem } from './MenuProductItem';

import { Box, Button, IconButton, Stack, Typography } from '@mui/material';
import AddCircleOutlineOutlinedIcon from '@mui/icons-material/AddCircleOutlineOutlined';
import { ProductItem } from '../Product/ProductItem';
import { RecieptBigItem } from '../Product/RecieptBigItem';
import { Swiper, SwiperSlide } from 'swiper/react';
import { FreeMode } from 'swiper/modules';

export const Column = ({
  id,
  title,
  items,
  onColumnChange,
  addProductHandler,
  selecteRecieptHandler,
  onRemoveItem,
  currentDay,
  fullMenu,
  reloadMenuhandler,
}) => {
  const [{ canDrop, isOver }, drop] = useDrop(() => ({
    accept: 'card',
    drop: (item, monitor) => {
      onColumnChange(item.id, id);
    },
    collect: (monitor) => ({
      isOver: monitor.isOver(),
      canDrop: monitor.canDrop(),
    }),
  }));

  return (
    <Box
      ref={drop}
      sx={{
        width: '100%',
        borderRadius: '5px',
        border: canDrop && isOver ? 'solid 1px green' : 'solid 1px #fff',
        borderBottom: canDrop && isOver ? 'solid 1px green' : 'solid 1px #eee',
        minHeight: '20vh',
        px: 0,
        mb: 2,
      }}
    >
      <Stack direction="row" spacing={2}>
        <Stack spacing={2} sx={{ px: 1, maxWidth: '200px' }}>
          <Box>
            <Typography sx={{ fontWeight: 'bold' }}>{title}</Typography>
          </Box>
          <Box>
            <Button
              color="success"
              variant="outlined"
              aria-label="add new product to day"
              onClick={() => addProductHandler(id)}
              size="small"
              startIcon={<AddCircleOutlineOutlinedIcon />}
            >
              Добавить
            </Button>
          </Box>
        </Stack>

        <Stack sx={{ m: 0, p: 0, width: '100%' }} direction="row" spacing={2}>
          <Swiper
            slidesPerView={4}
            spaceBetween={15}
            // freeMode={true}
            loop={true}
            // modules={[FreeMode]}
            className="mySwiper"
          >
            {items?.map((item) => (
              <SwiperSlide
                key={item.id}
                style={{ width: '200px', textAlign: 'left' }}
              >
                <MenuProductItem>
                  <RecieptBigItem
                    reciept={{ recipe: item }}
                    key={item.id}
                    showRemoveItem={true}
                    onClickToReciept={(item) => selecteRecieptHandler(item)}
                    onRemoveItem={() => onRemoveItem(item)}
                    currentDay={currentDay}
                    updateFromMenu={true}
                    fullMenu={fullMenu}
                    num_serv={item.num_serv}
                    reloadMenuhandler={reloadMenuhandler}
                  />
                </MenuProductItem>
              </SwiperSlide>
            ))}
          </Swiper>
          {canDrop && isOver && (
            <Box sx={{ width: '100%', textAlign: 'center' }}>
              <Typography sx={{ color: '#818181', fontSize: '12px' }}>
                'Перенести сюда 👇'
              </Typography>
            </Box>
          )}
        </Stack>
      </Stack>
    </Box>
  );
};
