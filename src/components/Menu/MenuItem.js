import { Box, Card, CardContent, Chip, Stack, Typography } from '@mui/material';
import React, { useEffect, useState } from 'react';
import styles from './Menu.module.scss';
import FormatListBulletedOutlinedIcon from '@mui/icons-material/FormatListBulletedOutlined';
import { useNavigate } from 'react-router-dom';

export const MenuItem = ({ id, menu }) => {
  const navigate = useNavigate();
  const [recipeCount, setRecipeCount] = useState(0);
  useEffect(() => {
    let count = 0;
    if (menu?.by_week_days) {
      for (const i in menu?.structured_menu) {
        count += menu?.structured_menu[i]?.length;
      }
    } else {
      count = menu?.unstructured_menu?.length;
    }
    setRecipeCount(count || 0);
  }, []);
  return (
    <Card
      className={styles.MenuItem}
      id={id}
      onClick={() => navigate(`/menu/${menu._id}`)}
    >
      <CardContent sx={{ zIndex: 1, position: 'relative' }}>
        {menu?.by_week_days ? (
          <Stack direction="row" spacing={1} sx={{ mb: 3 }}>
            <Chip
              size="small"
              label="Пн"
              sx={{ fontSize: '10px', borderRadius: '3px' }}
              color={
                menu?.structured_menu && menu?.structured_menu['mon']?.length
                  ? 'success'
                  : 'default'
              }
            />
            <Chip
              size="small"
              label="Вт"
              sx={{ fontSize: '10px', borderRadius: '3px' }}
              color={
                menu?.structured_menu && menu?.structured_menu['tues']?.length
                  ? 'success'
                  : 'default'
              }
            />
            <Chip
              size="small"
              label="Ср"
              sx={{ fontSize: '10px', borderRadius: '3px' }}
              color={
                menu?.structured_menu && menu?.structured_menu['wed']?.length
                  ? 'success'
                  : 'default'
              }
            />
            <Chip
              size="small"
              label="Чт"
              sx={{ fontSize: '10px', borderRadius: '3px' }}
              color={
                menu?.structured_menu && menu?.structured_menu['thur']?.length
                  ? 'success'
                  : 'default'
              }
            />
            <Chip
              size="small"
              label="Пт"
              sx={{ fontSize: '10px', borderRadius: '3px' }}
              color={
                menu?.structured_menu && menu?.structured_menu['fri']?.length
                  ? 'success'
                  : 'default'
              }
            />
            <Chip
              size="small"
              label="Сб"
              sx={{ fontSize: '10px', borderRadius: '3px' }}
              color={
                menu?.structured_menu && menu?.structured_menu['sat']?.length
                  ? 'success'
                  : 'default'
              }
            />
            <Chip
              size="small"
              label="Вс"
              sx={{ fontSize: '10px', borderRadius: '3px' }}
              color={
                menu?.structured_menu && menu?.structured_menu['sun']?.length
                  ? 'success'
                  : 'default'
              }
            />
          </Stack>
        ) : (
          <Stack
            sx={{ height: '24px', mb: 3 }}
            direction="row"
            spacing={2}
            alignItems="center"
          >
            <Box>
              <FormatListBulletedOutlinedIcon fontSize="small" />
            </Box>
            <Box>
              <Typography>Общее меню</Typography>
            </Box>
          </Stack>
        )}
        <Typography sx={{ pb: 0, mb: 0, lineHeight: 1 }} as="p" variant="h6">
          {menu?.title?.length > 20
            ? menu?.title?.slice(0, 20) + '...'
            : menu?.title || 'Меню без названия'}
        </Typography>
        <Stack
          direction="row"
          spacing={2}
          justifyContent="space-between"
          alignItems="center"
          sx={{ mt: 2 }}
        >
          <Chip
            size="small"
            label={`${recipeCount} рецепта`}
            color="success"
            sx={{ fontSize: '10px', borderRadius: '6px' }}
          />
          <Typography
            sx={{ pb: 0, mb: 0, lineHeight: 1 }}
            as="p"
            variant="caption"
          >
            17.06.2024
          </Typography>
        </Stack>
      </CardContent>
      <Box className={styles.MenuItem__After}></Box>
    </Card>
  );
};
