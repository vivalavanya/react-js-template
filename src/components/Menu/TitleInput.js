import React from 'react';

import { TextField } from '@mui/material';

export const TitleInput = (props) => {
  const { onSubmit, placeholder = 'New Item' } = props;

  const [title, setTitle] = React.useState('');
  const handleSubmit = (e) => {
    e.preventDefault();
    if (!title) return;
    onSubmit(title);
    setTitle('');
  };

  return (
    <form
      onSubmit={(e) => handleSubmit(e)}
      style={{ display: 'flex', margin: '0.1rem' }}
    >
      <TextField
        type="text"
        name="title"
        onChange={(e) => setTitle(e.target.value)}
        value={title}
        placeholder={placeholder}
      />
    </form>
  );
};
