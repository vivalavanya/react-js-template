import { useDrag } from 'react-dnd';

import {
  Box,
  Button,
  Card,
  CardContent,
  Checkbox,
  IconButton,
  Stack,
  Typography,
} from '@mui/material';
import DeleteOutlinedIcon from '@mui/icons-material/DeleteOutlined';

export const MenuProductItem = ({
  item,
  onNewItem,
  onRemoveItem,
  onClick,
  children,
}) => {
  const [collected, drag] = useDrag(() => ({
    type: 'card',
    item: item,
    end(item, monitor) {
      console.log('Dropped!', item);
    },
  }));

  return (
    <Box ref={drag} sx={{ mb: 1, mx: 0, cursor: 'grabbing !important' }}>
      {children}
    </Box>
    // <Card >
    //   <CardContent>
    //     <Stack
    //       direction="row"
    //       spacing={2}
    //       justifyContent="space-between"
    //       alignItems="center"
    //     >
    //       <Box onClick={() => onClick(item)}>
    //         <Typography as="p" sx={{ fontSize: '13px', lineHeight: 1 }}>
    //           {item.title?.length > 30
    //             ? item.title?.slice(0, 30) + '...'
    //             : item.title}
    //         </Typography>
    //       </Box>
    //       <Box>
    //         <Checkbox size="small" />
    //         {/* <IconButton
    //           color="inherit"
    //           aria-label="Remove product from day"
    //           onClick={() => onRemoveItem(item)}
    //           size="small"
    //           sx={{ fontSize: '8px', opacity: 0.4 }}
    //         >
    //           <DeleteOutlinedIcon />
    //         </IconButton> */}
    //       </Box>
    //     </Stack>
    //   </CardContent>
    // </Card>
  );
};
