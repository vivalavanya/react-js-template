import {
  Box,
  Checkbox,
  FormControlLabel,
  FormGroup,
  Stack,
  Typography,
} from '@mui/material';
import React from 'react';

export const RecieptIgredientsItem = ({ item, index }) => {
  return (
    <Stack direction="row" spacing={2}>
      <Box>
        <Typography
          as="p"
          variant="h6"
          sx={{ opacity: 0.3, fontWeight: 'bold' }}
        >
          {index}
        </Typography>
      </Box>
      <Box>
        <Typography as="p">{item.title}</Typography>
        <FormGroup>
          {item?.ingredients?.map((i) => (
            <FormControlLabel
              control={<Checkbox defaultChecked color="success" />}
              label={i.title}
            />
          ))}
        </FormGroup>
      </Box>
    </Stack>
  );
};
