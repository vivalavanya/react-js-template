import {
  Autocomplete,
  Box,
  Button,
  Checkbox,
  FormControlLabel,
  FormGroup,
  Stack,
  TextField,
  Typography,
} from '@mui/material';
import React, { useEffect, useState } from 'react';
import { useApi } from '../../hooks/useApi';
import { GetIngredients, GetManyRecipes } from '../../api/recipe.api';

export const Filter = ({
  version = 'small',
  setFilter,
  filter,
  filterButtonHandler,
}) => {
  const [ingredients, setIngredients] = useState([]);
  const GetIngredientsApi = useApi(GetIngredients);

  const GetIngredientsHandler = async ({ prefix }) => {
    const result = await GetIngredientsApi.sendRequest({ prefix });
    setIngredients(result);
  };

  const complexity = ['Легко', 'Средняя', 'Шеф'];
  const mealType = ['Легкий перекус', 'Завтрак', 'Обед', 'Ужин'];

  return (
    <Box
      sx={{
        background: '#f9f9f9',
        height: '100%',

        minWidth: version == 'small' ? '300px' : '0px',
      }}
    >
      <Stack
        justifyContent="space-around"
        sx={{ height: version == 'small' ? '100vh' : 'auto' }}
      >
        <Box
          sx={{
            py: 2,
            px: version == 'small' ? 4 : 2,
            height: version == 'small' ? '84vh' : 'auto',
            overflow: 'auto',
          }}
        >
          <Typography as="p" variant="h6" sx={{ mb: 4 }}>
            Фильтр
          </Typography>
          <TextField
            label="Поиск блюда"
            size="small"
            fullWidth
            onChange={({ target: { value } }) =>
              setFilter({ ...filter, title: value })
            }
          />

          <FormControlLabel
            sx={{ mt: 2 }}
            control={
              <Checkbox
                onChange={({ target: { checked } }) =>
                  setFilter({ ...filter, pro: checked })
                }
              />
            }
            label="Pro рецепт"
          />
          <Typography as="p" variant="h6" sx={{ mb: 1, mt: 3 }}>
            Сложность
          </Typography>
          <FormGroup>
            {complexity?.map((label) => (
              <FormControlLabel
                control={
                  <Checkbox
                    onChange={({ target: { checked } }) =>
                      setFilter({
                        ...filter,
                        complexity: checked
                          ? [...(filter?.complexity || []), label.toLowerCase()]
                          : filter?.complexity?.filter(
                              (c) => c !== label.toLowerCase(),
                            ),
                      })
                    }
                  />
                }
                label={label}
              />
            ))}
          </FormGroup>
          <Typography as="p" variant="h6" sx={{ mb: 1, mt: 3 }}>
            Время приготавления
          </Typography>
          <Stack direction="row" spacing={2}>
            <TextField
              label="От"
              size="small"
              onChange={({ target: { value } }) =>
                setFilter({
                  ...filter,
                  minutesDuration: {
                    ...(filter?.minutesDuration || {}),
                    gte: value,
                  },
                })
              }
            />
            <TextField
              label="До"
              size="small"
              onChange={({ target: { value } }) =>
                setFilter({
                  ...filter,
                  minutesDuration: {
                    ...(filter?.minutesDuration || {}),
                    lte: value,
                  },
                })
              }
            />
          </Stack>

          <Typography as="p" variant="h6" sx={{ mb: 1, mt: 3 }}>
            Тип еды
          </Typography>
          <FormGroup>
            {mealType?.map((label) => (
              <FormControlLabel
                control={
                  <Checkbox
                    onChange={({ target: { checked } }) =>
                      setFilter({
                        ...filter,
                        mealType: checked
                          ? [...(filter?.mealType || []), label?.toLowerCase()]
                          : filter?.mealType?.filter(
                              (c) => c !== label?.toLowerCase(),
                            ),
                      })
                    }
                  />
                }
                label={label}
              />
            ))}
          </FormGroup>

          <Typography as="p" variant="h6" sx={{ mb: 1, mt: 3 }}>
            Ингредиент
          </Typography>
          <Autocomplete
            multiple
            id="in"
            sx={{ mt: 2 }}
            options={ingredients}
            onChange={(event, newValue) => {
              setFilter({
                ...filter,
                ingredients: { ...(filter?.ingredients || {}), in_: newValue },
              });
            }}
            getOptionLabel={(option) => option}
            renderInput={(params) => (
              <TextField
                {...params}
                variant="outlined"
                size="small"
                onChange={({ target: { value } }) =>
                  GetIngredientsHandler({ prefix: value })
                }
                label="Обазательные"
                placeholder="Обазательные"
              />
            )}
          />
          <Autocomplete
            multiple
            id="nin"
            sx={{ mt: 2 }}
            options={ingredients}
            onChange={(event, newValue) => {
              setFilter({
                ...filter,
                ingredients: { ...(filter?.ingredients || {}), nin_: newValue },
              });
            }}
            getOptionLabel={(option) => option}
            renderInput={(params) => (
              <TextField
                {...params}
                variant="outlined"
                size="small"
                onChange={({ target: { value } }) =>
                  GetIngredientsHandler({ prefix: value })
                }
                label="Исключить"
                placeholder="Исключить ингредиенты"
              />
            )}
          />
        </Box>
        <Box sx={{ p: 2, background: '#f1f1f1', mt: 'auto' }}>
          <Button
            variant="contained"
            color="success"
            fullWidth
            onClick={filterButtonHandler}
          >
            Отфильтровать
          </Button>
        </Box>
      </Stack>
    </Box>
  );
};
