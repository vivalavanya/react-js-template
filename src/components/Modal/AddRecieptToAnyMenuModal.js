import {
  Box,
  Button,
  Fab,
  FormControl,
  InputLabel,
  MenuItem,
  Modal,
  Select,
  Stack,
  TextField,
  Typography,
} from '@mui/material';
import React, { useEffect, useState } from 'react';
import styles from './Modal.module.scss';
import ClearOutlinedIcon from '@mui/icons-material/ClearOutlined';
import { CreateMenu, GetManyMenu, UpadteMenu } from '../../api/menu.api';
import { useApi } from '../../hooks/useApi';
import { useDispatch } from 'react-redux';
import { setAlert } from '../../store/alert.store';

export const AddRecieptToAnyMenuModal = ({ open, onClose, selectedRecipe }) => {
  const [selectedMenu, setSelectedMenu] = useState('');
  const [step, setStep] = useState(1);
  const [currentMenu, setCurrentMenu] = useState({});
  const [byWeekDays, setByWeekDays] = useState(false);
  const [menuTitle, setMenuTitle] = useState('');
  const [menus, setMenus] = useState([]);
  const CreateMenuApi = useApi(CreateMenu);
  const GetManyMenuApi = useApi(GetManyMenu);

  const GetManyMenuHandler = async () => {
    const result = await GetManyMenuApi.sendRequest({
      limit: 99999999,
      offset: 0,
    });
    setMenus(result.data);
  };

  useEffect(() => {
    setCurrentMenu(menus?.find((menu) => menu?._id == selectedMenu));
  }, [selectedMenu]);
  useEffect(() => {
    GetManyMenuHandler();
  }, []);
  const [selectedDay, setSelectedDay] = useState('mon');
  const UpdateMenuApi = useApi(UpadteMenu);
  const dispatch = useDispatch();
  const UpdateMenuHandler = async (data) => {
    await UpdateMenuApi.sendRequest(data, selectedMenu);
    dispatch(setAlert({ text: 'Рецент успешно добавлен в меню', status: 200 }));
    onClose();
  };

  return (
    <Modal
      open={open}
      onClose={onClose}
      aria-labelledby="modal-modal-title"
      aria-describedby="modal-modal-description"
      sx={{ display: 'flex', alignItems: 'center', justifyContent: 'center' }}
    >
      <Box className={styles.HelloModal} sx={{ position: 'relative' }}>
        <Fab
          variant="contained"
          color="error"
          size="small"
          sx={{ position: 'absolute', right: '15px', top: '15px' }}
          onClick={onClose}
        >
          <ClearOutlinedIcon />
        </Fab>
        <Stack alignItems="center">
          {step === 1 && (
            <>
              <img alt="add reciept to menu" src="/add-to-menu.jpg" />
              <Typography
                as="h3"
                variant="h5"
                sx={{ fontWeight: 'bold', mt: 2 }}
              >
                Добавьте рецепт в меню
              </Typography>

              <Typography id="modal-modal-description">
                Выберите существующее меню или создайте новое
              </Typography>
              <FormControl sx={{ mt: 3 }} fullWidth>
                <InputLabel id="demo-simple-select-label">
                  Выберите меню
                </InputLabel>

                <Select
                  labelId="demo-simple-select-label"
                  id="demo-simple-select"
                  value={menus?.find((m) => m._id == selectedMenu)?.id}
                  label="Выберите меню"
                  onChange={({ target: { value } }) => setSelectedMenu(value)}
                >
                  {menus?.map((menu) => (
                    <MenuItem key={menu?._id} value={menu._id}>
                      {menu?.title || 'Меню без названия'}
                    </MenuItem>
                  ))}
                </Select>
              </FormControl>

              {menus?.find((m) => m._id == selectedMenu)?.by_week_days ? (
                <Button
                  variant="contained"
                  color="success"
                  sx={{ mt: 2 }}
                  fullWidth
                  onClick={() => setStep(2)}
                >
                  Продолжить
                </Button>
              ) : (
                <Button
                  variant="contained"
                  color="success"
                  sx={{ mt: 2 }}
                  fullWidth
                  onClick={() => {
                    if (selectedRecipe?.numServ) {
                      delete selectedRecipe.numServ;
                    }
                    UpdateMenuHandler({
                      unstructured_menu: [
                        ...(menus?.find((m) => m._id == selectedMenu)
                          ?.unstructured_menu || []),
                        {
                          ...selectedRecipe,
                          num_serv: selectedRecipe?.numServ || 1,
                        },
                      ],
                    });
                  }}
                >
                  Добавить в меню
                </Button>
              )}
              <Button
                variant="text"
                color="primary"
                sx={{ mt: 1 }}
                fullWidth
                onClick={() => setStep(3)}
              >
                Создать меню
              </Button>
            </>
          )}

          {step == 2 && (
            <>
              <img
                alt="add reciept to menu"
                src="/select-calendar.jpg"
                style={{ width: '90%' }}
              />
              <Typography
                as="h3"
                variant="h5"
                sx={{ fontWeight: 'bold', mt: 2 }}
              >
                Выберите день
              </Typography>

              <Typography id="modal-modal-description">
                Выберите день недели, в которые добавите рецепт
              </Typography>
              <FormControl sx={{ mt: 3 }} fullWidth>
                <InputLabel id="demo-simple-select-label">
                  Выберите день недели
                </InputLabel>
                <Select
                  labelId="demo-simple-select-label"
                  id="demo-simple-select"
                  value={selectedDay}
                  label="Выберите день недели"
                  onChange={({ target: { value } }) => setSelectedDay(value)}
                >
                  <MenuItem value="mon">Понедельник</MenuItem>
                  <MenuItem value="tues">Вторник</MenuItem>
                  <MenuItem value="wen">Среда</MenuItem>
                  <MenuItem value="thur">Четверг</MenuItem>
                  <MenuItem value="fri">Пятница</MenuItem>
                  <MenuItem value="sat">Суббота</MenuItem>
                  <MenuItem value="sun">Воскресенье</MenuItem>
                </Select>
              </FormControl>
              <Button
                variant="contained"
                color="success"
                sx={{ mt: 2 }}
                fullWidth
                onClick={() => {
                  UpdateMenuHandler({
                    structured_menu: {
                      ...(currentMenu?.structured_menu || {}),
                      [selectedDay]: [
                        ...(currentMenu?.structured_menu[selectedDay] || []),
                        selectedRecipe,
                      ],
                    },
                  });
                }}
              >
                Добавить рецепт
              </Button>
              <Button
                variant="text"
                color="primary"
                sx={{ mt: 1 }}
                fullWidth
                onClick={() => setStep(1)}
              >
                Назад
              </Button>
            </>
          )}

          {step == 3 && (
            <>
              <img
                alt="add reciept to menu"
                src="/create-menu.jpg"
                style={{ width: '100%' }}
              />
              <Typography
                as="h3"
                variant="h5"
                sx={{ fontWeight: 'bold', mt: 2 }}
              >
                Создайте меню
              </Typography>

              <Typography id="modal-modal-description">
                Создайте ваше новое меню и добавьте в него рецепт
              </Typography>
              <TextField
                label="Название"
                fullWidth
                sx={{ mt: 3 }}
                value={menuTitle}
                onChange={({ target: { value } }) => setMenuTitle(value)}
              />
              <FormControl sx={{ mt: 3 }} fullWidth>
                <InputLabel id="demo-simple-select-label">
                  Выберите день недели
                </InputLabel>
                <Select
                  labelId="demo-simple-select-label"
                  id="demo-simple-select"
                  value={byWeekDays}
                  label="Выберите день недели"
                  onChange={({ target: { value } }) => setByWeekDays(value)}
                >
                  <MenuItem value={true}>Меню по дням</MenuItem>
                  <MenuItem value={false}>Общее меню</MenuItem>
                </Select>
              </FormControl>
              <Button
                variant="contained"
                color="success"
                sx={{ mt: 2 }}
                fullWidth
                disabled={!menuTitle}
                onClick={() => {
                  CreateMenuApi.sendRequest({
                    by_week_days: byWeekDays,
                    title: menuTitle,
                  }).then((result) => {
                    setSelectedMenu(result);
                    if (byWeekDays) {
                      setStep(2);
                    } else {
                      UpdateMenuHandler({
                        unstructured_menu: [
                          ...(menus?.find((m) => m._id == selectedMenu)
                            ?.unstructured_menu || []),
                          {
                            ...selectedRecipe,
                            num_serv: selectedRecipe.numServ,
                          },
                        ],
                      });
                      onClose();
                    }
                  });
                }}
              >
                Продолжить
              </Button>
              <Button
                variant="text"
                color="primary"
                sx={{ mt: 1 }}
                fullWidth
                onClick={() => setStep(1)}
              >
                Назад
              </Button>
            </>
          )}
        </Stack>
      </Box>
    </Modal>
  );
};
