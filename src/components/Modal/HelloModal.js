import React, { useState } from 'react';
import styles from './Modal.module.scss';
import { Box, Button, Modal, Stack, Typography } from '@mui/material';
import { useCookies } from 'react-cookie';
import { add } from 'date-fns';
import { useDispatch } from 'react-redux';
import { setInstruction } from '../../store/instruction.store';
export const HelloModal = ({ startLearningHandler }) => {
  const [open, setOpen] = useState(true);
  const [cookie, setCookie] = useCookies('instruction');
  const dispatch = useDispatch();
  return (
    <Modal
      open={open}
      sx={{ display: 'flex', alignItems: 'center', justifyContent: 'center' }}
    >
      <Box className={styles.HelloModal}>
        <img alt="welcome" src="/welcome.png" />
        <Typography as="h3" variant="h5" sx={{ fontWeight: 'bold', mt: 5 }}>
          Добро пожаловать в Bystems!
        </Typography>
        <Typography sx={{ mt: 3 }}>
          Мы подготовили для вас короткую инструкцию по работе с системой!
        </Typography>
        <Stack sx={{ mt: 3 }}>
          <Button
            variant="contained"
            color="success"
            onClick={() => {
              setCookie('instruction', true, {
                path: '/',
              });
              setOpen(false);
              dispatch(setInstruction({ step: 'userProfile' }));
            }}
          >
            Начать обучение
          </Button>
          <Button
            size="small"
            sx={{ mt: 1 }}
            onClick={() => {
              setOpen(false);
              setCookie('instruction', false, {
                path: '/',
              });
              dispatch(setInstruction({ step: null }));
            }}
          >
            Продолжить без обучения
          </Button>
        </Stack>
      </Box>
    </Modal>
  );
};
