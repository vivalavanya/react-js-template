import {
  AppBar,
  Avatar,
  Box,
  Button,
  Grid,
  IconButton,
  ListItemIcon,
  ListItemText,
  Menu,
  MenuItem,
  Toolbar,
  Typography,
} from '@mui/material';
import React, { useState } from 'react';
import { Link, useLocation, useNavigate } from 'react-router-dom';
import { url } from '../../../utils/url';
import TipsAndUpdatesOutlinedIcon from '@mui/icons-material/TipsAndUpdatesOutlined';
import { InterfaceInstructionItem } from '../../InterfaceInstruction/InterfaceInstructionItem';
import { useDispatch } from 'react-redux';
import { setInstruction } from '../../../store/instruction.store';
import MenuIcon from '@mui/icons-material/Menu';
import styles from './Header.module.scss';
import { BorderBottom } from '@mui/icons-material';
import CalendarMonthOutlinedIcon from '@mui/icons-material/CalendarMonthOutlined';
import FormatListBulletedOutlinedIcon from '@mui/icons-material/FormatListBulletedOutlined';
import { useApi } from '../../../hooks/useApi';
import { CreateMenu } from '../../../api/menu.api';
import AuthService from '../../../api/keycloak.api';

export const Header = () => {
  const CreateMenuApi = useApi(CreateMenu);

  const navigate = useNavigate();
  const dispatch = useDispatch();
  const location = useLocation();

  const [anchorElNav, setAnchorElNav] = React.useState(null);
  const [anchorElUser, setAnchorElUser] = React.useState(null);
  const openUser = Boolean(anchorElUser);

  const [anchorEl, setAnchorEl] = React.useState(null);
  const open = Boolean(anchorEl);
  const handleClick = (event) => {
    setAnchorEl(event.currentTarget);
  };
  const handleClose = () => {
    setAnchorEl(null);
  };

  const handleOpenNavMenu = (event) => {
    setAnchorElNav(event.currentTarget);
  };
  const handleOpenUserMenu = (event) => {
    setAnchorElUser(event.currentTarget);
  };

  const handleCloseNavMenu = () => {
    setAnchorElNav(null);
  };

  const handleClickUser = (event) => {
    setAnchorElUser(event.currentTarget);
  };

  const logout = () => {
    AuthService.doLogout();
  };

  const CreateMenuHandler = async ({ by_week_days }) => {
    const result = await CreateMenuApi.sendRequest({
      by_week_days,
    });
    if (result) {
      navigate(`/menu/${result}`);
    }
  };

  return (
    <AppBar
      sx={{ p: 0, mx: 0, mb: 5, boxShadow: 'none' }}
      color="inherit"
      position="static"
    >
      <Toolbar sx={{ p: '0 !important', mx: 0 }}>
        <Box sx={{ flexGrow: 1 }} component="div">
          <Link
            to={url.dashboard}
            style={{ textDecoration: 'none', color: '#000' }}
          >
            <Link to={url.menu}>
              <img src="/logo.png" style={{ width: '135px' }} />
            </Link>
          </Link>
        </Box>

        <Box sx={{ flexGrow: 1, display: { xs: 'flex', md: 'none' } }}>
          <IconButton
            size="large"
            aria-label="account of current user"
            aria-controls="menu-appbar"
            aria-haspopup="true"
            onClick={handleOpenNavMenu}
            color="inherit"
          >
            <MenuIcon />
          </IconButton>
          <Menu
            id="menu-appbar"
            anchorEl={anchorElNav}
            anchorOrigin={{
              vertical: 'bottom',
              horizontal: 'left',
            }}
            keepMounted
            transformOrigin={{
              vertical: 'top',
              horizontal: 'left',
            }}
            open={Boolean(anchorElNav)}
            onClose={handleCloseNavMenu}
            sx={{
              display: { xs: 'block', md: 'none' },
            }}
          >
            <MenuItem>
              <Typography textAlign="center">Меню</Typography>
            </MenuItem>
            <MenuItem>
              <Typography textAlign="center">Рецепты</Typography>
            </MenuItem>
          </Menu>
        </Box>

        <Box
          sx={{ flexGrow: 1, display: { xs: 'none', md: 'flex' } }}
          className={styles.Header__Menu}
        >
          <Link
            to={url.menu}
            className={
              location.pathname.indexOf('/menu') > -1
                ? styles.Header__MenuItem
                : ''
            }
          >
            Меню
          </Link>
          <Link
            to={url.reciepts}
            className={
              location.pathname.indexOf('/reciepts') > -1
                ? styles.Header__MenuItem
                : ''
            }
          >
            Рецепты
          </Link>
          <Link
            to={url.wishlist}
            className={
              location.pathname.indexOf('/wishlist') > -1
                ? styles.Header__MenuItem
                : ''
            }
          >
            Избранное
          </Link>
        </Box>

        <IconButton
          id="ViewInstructionAlways"
          color="inherit"
          aria-label="Remove product from day"
          sx={{ fontSize: '8px', mr: 2 }}
          onClick={() => dispatch(setInstruction({ step: 'userProfile' }))}
        >
          <TipsAndUpdatesOutlinedIcon />
        </IconButton>
        <Button
          id="CreateFirstMenu"
          color="success"
          variant="outlined"
          aria-controls={open ? 'basic-menu' : undefined}
          aria-haspopup="true"
          aria-expanded={open ? 'true' : undefined}
          onClick={handleClick}
        >
          Добавить меню
        </Button>
        <Menu
          id="basic-menu"
          anchorEl={anchorEl}
          open={open}
          onClose={handleClose}
          MenuListProps={{
            'aria-labelledby': 'basic-button',
          }}
        >
          <MenuItem onClick={() => CreateMenuHandler({ by_week_days: true })}>
            <ListItemIcon>
              <CalendarMonthOutlinedIcon fontSize="small" />
            </ListItemIcon>
            <ListItemText>Меню по дням</ListItemText>
          </MenuItem>
          <MenuItem onClick={() => CreateMenuHandler({ by_week_days: false })}>
            <ListItemIcon>
              <FormatListBulletedOutlinedIcon fontSize="small" />
            </ListItemIcon>
            <ListItemText>Общее меню</ListItemText>
          </MenuItem>
        </Menu>
        <Avatar id="userProfile" sx={{ ml: 2 }} onClick={handleClickUser}>
          N
        </Avatar>
        <Menu
          id="basic-menu-user"
          anchorEl={anchorElUser}
          open={openUser}
          onClose={() => setAnchorElUser(null)}
          MenuListProps={{
            'aria-labelledby': 'basic-button',
          }}
        >
          <MenuItem onClick={() => logout()}>Выход</MenuItem>
        </Menu>
        <InterfaceInstructionItem
          step="userProfile"
          title="Упарвляйте своим профилем от сюда."
          actionHandler={() =>
            dispatch(setInstruction({ step: 'CreateFirstMenu' }))
          }
          actionText="Продолжить"
        />
        <InterfaceInstructionItem
          step="CreateFirstMenu"
          title="Создайте ваше первое меню"
          actionHandler={() =>
            dispatch(setInstruction({ step: 'ViewDemoMenu' }))
          }
          actionText="Продолжить"
        />
        <InterfaceInstructionItem
          step="ViewInstructionAlways"
          title="Вы всегда можете посмотреть инструкцию нажав на эту кнопку!"
          actionHandler={() => dispatch(setInstruction({ step: null }))}
          actionText="Завершить"
        />
      </Toolbar>
    </AppBar>
  );
};
