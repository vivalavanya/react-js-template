import { Box, Button, Modal, Stack, Typography } from '@mui/material';
import React from 'react';
import ErrorOutlinedIcon from '@mui/icons-material/ErrorOutlined';
export const ActionApprove = ({
  open,
  onClose,
  onSuccess,
  title,
  description,
}) => {
  return (
    <Modal
      open={open}
      onClose={onClose}
      aria-labelledby="modal-modal-title"
      aria-describedby="modal-modal-description"
      sx={{ display: 'flex', alignItems: 'center', justifyContent: 'center' }}
    >
      <Box
        sx={{
          width: '100%',
          maxWidth: '400px',
          p: 4,
          height: 'auto',
          borderRadius: '5px',
          background: '#fff',
        }}
      >
        <Stack direction="row" spacing={1} alignItems="center">
          <ErrorOutlinedIcon color="warning" />
          <Typography id="modal-modal-title" variant="h6" component="h2">
            {title}
          </Typography>
        </Stack>
        <Typography id="modal-modal-description" sx={{ mt: 2 }}>
          {description}
        </Typography>
        <Stack direction="row" spacing={2} sx={{ mt: 2 }}>
          <Button variant="outlined" color="error" onClick={onClose}>
            Закрыть
          </Button>
          <Button variant="contained" color="success" onClick={onSuccess}>
            Подтвердить
          </Button>
        </Stack>
      </Box>
    </Modal>
  );
};
