import { Container, Grid } from '@mui/material';
import React from 'react';
import { Header } from '../Header/Header';
import { useSelector } from 'react-redux';
import { AlertsList } from '../AlertList/AlertList';

export const MainLayout = ({ children }) => {
  const alerts = useSelector((state) => state.alerts);

  return (
    <Container maxWidth="lg">
      <Header />

      {children}
      <AlertsList alerts={alerts} />
    </Container>
  );
};
