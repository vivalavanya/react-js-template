import {
  Box,
  Card,
  CardContent,
  Container,
  Grid,
  Stack,
  Typography,
} from '@mui/material';
import React from 'react';
import { Link } from 'react-router-dom';

export const SignLayout = ({ title, children }) => {
  return (
    <Container maxWidth="lg">
      <Grid container spacing={2}>
        <Grid
          item
          xs={12}
          m={10}
          lg={5}
          sx={{ justifyContent: 'center', alignItems: 'center', mx: 'auto' }}
        >
          <Card variant="outlined" sx={{ borderRadius: '10px' }}>
            <CardContent>
              <Typography
                as="h1"
                variant="h5"
                sx={{ textAlign: 'center', fontWeight: 'bold', mb: 2 }}
              >
                {title}
              </Typography>
              {children}
              <Box>
                <Stack
                  direction="row"
                  spacing={2}
                  sx={{ alignItems: 'center', justifyContent: 'center' }}
                >
                  <Box>
                    <Link
                      style={{
                        color: '#818181',
                        textDecoration: 'none',
                      }}
                      to="/"
                    >
                      Вход
                    </Link>
                  </Box>
                  <Box>
                    <Link
                      style={{
                        color: '#818181',
                        textDecoration: 'none',
                      }}
                      to="/"
                    >
                      Восстановление пароля
                    </Link>
                  </Box>
                </Stack>
                <Stack
                  direction="row"
                  spacing={2}
                  sx={{ alignItems: 'center', justifyContent: 'center', mt: 1 }}
                >
                  <Box>
                    <Link
                      style={{
                        color: '#818181',
                        textDecoration: 'none',
                      }}
                      to="/"
                    >
                      Политика конфиденциальности
                    </Link>
                  </Box>
                </Stack>
              </Box>
            </CardContent>
          </Card>
        </Grid>
      </Grid>
    </Container>
  );
};
