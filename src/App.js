import '@fontsource/roboto/300.css';
import '@fontsource/roboto/400.css';
import '@fontsource/roboto/500.css';
import '@fontsource/roboto/700.css';

import './App.css';
import { ThemeProvider, createTheme } from '@mui/material';
import { Route, BrowserRouter as Router, Routes } from 'react-router-dom';
import { url } from './utils/url';
import { HomePage } from './pages/HomePage';
import { MenusPage } from './pages/MenusPage';
import { SingleMenuPage } from './pages/SingleMenuPage';
import { RecieptsPage } from './pages/RecieptsPage';
import 'swiper/css';
import { WishlistPage } from './pages/WishlistPage';
import { LoginPage } from './pages/LoginPage';
import { RegistrationPage } from './pages/RegistrationPage';

const theme = createTheme({
  palette: {
    primary: {
      main: '#303E9F',
      light: '#303E9F',
      dark: '#1d276c',
      contrastText: '#fff',
    },
    secondary: {
      main: '#303E9F12',
      light: '#4b5ac612',
      dark: '#29358712',
      contrastText: '#303E9F',
    },
  },
});

function App() {
  return (
    <ThemeProvider theme={theme}>
      <Router>
        <Routes>
          <Route exact path={url.home} element={<HomePage />} />
          <Route exact path={url.menu} element={<MenusPage />} />
          <Route exact path={url.singleMenu} element={<SingleMenuPage />} />
          <Route exact path={url.addMenu} element={<SingleMenuPage />} />
          <Route exact path={url.reciepts} element={<RecieptsPage />} />
          <Route exact path={url.wishlist} element={<WishlistPage />} />
          <Route exact path={url.login} element={<LoginPage />} />
          <Route exact path={url.registration} element={<RegistrationPage />} />
        </Routes>
      </Router>
    </ThemeProvider>
  );
}

export default App;
