import { createSlice } from '@reduxjs/toolkit';
import { v4 as uuidv4 } from 'uuid';

export const instructionSlice = createSlice({
  name: 'instruction',
  initialState: {
    step: null,
  },
  reducers: {
    setInstruction: (state, action) => {
      return action?.payload;
    },
  },
});
export const { setInstruction } = instructionSlice.actions;
export default instructionSlice.reducer;
