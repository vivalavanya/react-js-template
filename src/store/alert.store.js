import { createSlice } from '@reduxjs/toolkit';
import { v4 as uuidv4 } from 'uuid';
const humanReadbleError = (error, statusCode) => {
  let text = '';
  switch (statusCode) {
    case 500:
      text = 'При обработке запроса произошла ошибка на сервере';
      break;
    case 502:
      text = 'Внутреняя ошибка сервера. Возможно сервер перегружен.';
      break;
    case 504:
      text = 'Сервер не смог отдать ответ в отведенное время. Таймаут запроса';
      break;
    case 400:
      text =
        'Вы отправили неверные данные. Сервер не может их обработать верно. Проверьте корректность введенных данных.';
      break;
    case 401:
      text = 'Для получения доступа к данным необходима авторизация';
      break;
    case 403:
      text = 'У вас недостаточно прав для просмотра данных материалов';
      break;
    case 404:
      text = 'Данные не найдены';
      break;
    case 200:
      text = error.text;
      break;
    case 201:
      text = error.text;
      break;
    default:
      break;
  }
  return text;
};
export const alertSlice = createSlice({
  name: 'user',
  initialState: [],
  reducers: {
    setAlert: (state, action) => {
      const error = action?.payload?.response?.data;
      if (action?.payload[0]?.id) {
        action.payload[0].id = uuidv4();
      }
      let data = [];
      const statusCode =
        error?.statusCode ||
        action?.payload?.response?.status ||
        action?.payload?.statusCode ||
        action?.payload?.status ||
        500;

      if (typeof action?.payload == 'object' && action?.payload?.length) {
        data = {
          id: uuidv4(),
          text: humanReadbleError(action?.payload, statusCode),
          type: [201, 200].includes(statusCode) ? 'success' : 'error',
          details: error?.detail || action?.payload?.detail,
          message: error?.message,
        };
      } else if (typeof action?.payload == 'object') {
        data = [
          {
            id: uuidv4(),
            text: humanReadbleError(action?.payload, statusCode),
            type: [201, 200].includes(statusCode) ? 'success' : 'error',
            details: error?.detail || action?.payload?.detail,
            message: error?.message,
          },
        ];
      }

      return [...state, ...data];
    },
    removeAlert: (state, action) => {
      const newState = state.filter((s, index) => s.id !== action.payload.id);
      return newState;
    },
  },
});
export const { setAlert, removeAlert } = alertSlice.actions;
export default alertSlice.reducer;
