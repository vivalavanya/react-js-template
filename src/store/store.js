import { configureStore } from '@reduxjs/toolkit';
import alertReducer from './alert.store';
import instructionSlile from './instruction.store';

export default configureStore({
  reducer: {
    alerts: alertReducer,
    instruction: instructionSlile,
  },
});
