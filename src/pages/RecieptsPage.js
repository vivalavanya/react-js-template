import { Box, Drawer, Grid, Modal, Typography } from '@mui/material';
import React, { useEffect, useState } from 'react';
import { MainLayout } from '../components/Default/Layout/MainLayout';
import { useApi } from '../hooks/useApi';

import { RecieptBigItem } from '../components/Product/RecieptBigItem';
import { Filter } from '../components/Filter/Filter';
import { RecieptFullData } from '../components/Product/RecieptFullData';
import { AddRecieptToAnyMenuModal } from '../components/Modal/AddRecieptToAnyMenuModal';
import { GetManyRecipes } from '../api/recipe.api';
import SearchOffIcon from '@mui/icons-material/SearchOff';

export const RecieptsPage = () => {
  const [selectedReciept, setSelectedReciept] = useState(null);
  const [addCandidate, setAddCandidate] = useState(null);
  const [recipes, setRecipes] = useState([]);
  const [addMenuModal, setAddMenuModal] = useState(false);

  const GetManyRecipesApi = useApi(GetManyRecipes);
  const [filter, setFilter] = useState();
  const [offset, setOffset] = useState(0);
  const [recipesTotalCount, setRecipesTotalCount] = useState(0);

  const GetManyRecipesHandler = async () => {
    const result = await GetManyRecipesApi.sendRequest({
      limit: 16,
      offset,
      filter,
      // sorting
    });

    setRecipes(result.data);
    setRecipesTotalCount(result.totalItems);
  };

  useEffect(() => {
    GetManyRecipesHandler();
  }, [offset]);

  // const GetManyRecieptsHandler = async () => {
  //   const result = await GetManyRecieptsApi.sendRequest();
  //   setReciepts(result);
  // };

  return (
    <MainLayout>
      <Grid container spacing={2}>
        <Grid item xs={12}>
          <Typography as="h1" variant="h4" sx={{ fontWeight: 'bold', mb: 5 }}>
            🥙 Список рецептов
          </Typography>
        </Grid>
      </Grid>
      <Grid container spacing={2}>
        <Grid item xs={3}>
          <Filter
            version="big"
            filter={filter}
            setFilter={(data) => setFilter(data)}
            filterButtonHandler={() => GetManyRecipesHandler()}
          />
        </Grid>
        <Grid item xs={9}>
          <Grid container spacing={2}>
            {recipes?.length ? (
              recipes?.map((reciept) => (
                <Grid item xs={4}>
                  <RecieptBigItem
                    reciept={{ recipe: reciept }}
                    onClickToReciept={(reciept) => setSelectedReciept(reciept)}
                    showCounts={false}
                    addToMenuHandler={(data) => {
                      setAddCandidate(data);

                      setAddMenuModal(true);
                    }}
                  />
                </Grid>
              ))
            ) : (
              <Grid item xs={6} sx={{ mx: 'auto', textAlign: 'center' }}>
                <SearchOffIcon />
                <Typography>
                  Рецепты не найдены, попробуйте изменить условия поиска
                </Typography>
              </Grid>
            )}
          </Grid>
        </Grid>
      </Grid>

      <Drawer
        anchor="right"
        open={selectedReciept}
        sx={{ maxHeight: '100vh' }}
        onClose={() => setSelectedReciept(null)}
      >
        <RecieptFullData
          hiddenButtons={true}
          reciept={selectedReciept}
          showCounts={false}
        />
      </Drawer>
      <AddRecieptToAnyMenuModal
        selectedRecipe={addCandidate}
        open={addMenuModal}
        onClose={() => setAddMenuModal(false)}
      />
    </MainLayout>
  );
};
