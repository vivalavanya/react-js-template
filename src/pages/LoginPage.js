import React, { useState } from 'react';
import { MainLayout } from '../components/Default/Layout/MainLayout';
import {
  Box,
  Button,
  Card,
  CardContent,
  Container,
  Grid,
  Stack,
  TextField,
  Typography,
} from '@mui/material';
import { Link } from 'react-router-dom';
import { SignLayout } from '../components/Default/Layout/SignLayout';
import AuthService from '../api/keycloak.api';

export const LoginPage = ({ title, children }) => {
  const [form, setForm] = useState({});

  return (
    <SignLayout title="Авторизация">
      <TextField
        fullWidth
        sx={{ mb: 2 }}
        label="Ваш e-mail"
        type="email"
        onChange={({ target: { value } }) => setForm({ ...form, email: value })}
      />
      <TextField
        fullWidth
        sx={{ mb: 2 }}
        label="Ваш пароль"
        type="password"
        onChange={({ target: { value } }) =>
          setForm({ ...form, password: value })
        }
      />
      <Button
        color="success"
        variant="contained"
        fullWidth
        sx={{ mb: 2 }}
        disabled={!form?.email || !form?.password}
        // onClick={() => ({
        //   ''
        // })}
      >
        Войти
      </Button>
    </SignLayout>
  );
};
