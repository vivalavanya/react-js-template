import { Drawer, Grid, Typography } from '@mui/material';
import React, { useEffect, useState } from 'react';
import { MainLayout } from '../components/Default/Layout/MainLayout';
import { useApi } from '../hooks/useApi';
// import { GetManyReciepts } from '../api/menu.api';
import { RecieptBigItem } from '../components/Product/RecieptBigItem';
import { AddRecieptToAnyMenuModal } from '../components/Modal/AddRecieptToAnyMenuModal';
import { RecieptFullData } from '../components/Product/RecieptFullData';
import { FavoritesRecipe } from '../api/recipe.api';
import SearchOffIcon from '@mui/icons-material/SearchOff';

export const WishlistPage = () => {
  const FavoritesRecipeApi = useApi(FavoritesRecipe);
  const [selectedReciept, setSelectedReciept] = useState(null);
  const [recipes, setRecipes] = useState([]);
  const [addMenuModal, setAddMenuModal] = useState(false);
  const [addCandidate, setAddCandidate] = useState(null);
  const GetManyRecieptsHandler = async () => {
    const { data } = await FavoritesRecipeApi.sendRequest();
    setRecipes(data);
  };

  useEffect(() => {
    GetManyRecieptsHandler();
  }, []);
  return (
    <MainLayout>
      <Grid container spacing={2}>
        <Grid item xs={12}>
          <Typography as="h1" variant="h4" sx={{ fontWeight: 'bold', mb: 5 }}>
            ❤️ Избранное
          </Typography>
        </Grid>
        <Grid item xs={12}>
          <Grid container spacing={2}>
            {recipes?.length ? (
              recipes?.map((reciept) => (
                <Grid item xs={3}>
                  <RecieptBigItem
                    reciept={{ recipe: reciept }}
                    onClickToReciept={(reciept) => setSelectedReciept(reciept)}
                    showCounts={false}
                    addToMenuHandler={(data) => {
                      setAddCandidate(data);

                      setAddMenuModal(true);
                    }}
                  />
                </Grid>
              ))
            ) : (
              <Grid item xs={12} sx={{ mx: 'auto', textAlign: 'center' }}>
                <SearchOffIcon />
                <Typography>Вы еще не добавили избранные рецепты</Typography>
              </Grid>
            )}
          </Grid>
        </Grid>
      </Grid>
      <Drawer
        anchor="right"
        open={selectedReciept}
        sx={{ maxHeight: '100vh' }}
        onClose={() => setSelectedReciept(null)}
      >
        <RecieptFullData
          showCounts={false}
          hiddenButtons={true}
          reciept={selectedReciept}
        />
      </Drawer>
      <AddRecieptToAnyMenuModal
        open={addMenuModal}
        selectedRecipe={addCandidate}
        onClose={() => setAddMenuModal(false)}
      />
    </MainLayout>
  );
};
