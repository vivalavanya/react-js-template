import React, { useEffect, useState } from 'react';
import { MainLayout } from '../components/Default/Layout/MainLayout';
import {
  Box,
  Button,
  Drawer,
  Grid,
  IconButton,
  Stack,
  Typography,
} from '@mui/material';
import { DndProvider } from 'react-dnd';
import { HTML5Backend } from 'react-dnd-html5-backend';
import { v4 as uuid } from 'uuid';
import { Column } from '../components/Menu/Column';

import { ProductItem } from '../components/Product/ProductItem';

import HighlightOffIcon from '@mui/icons-material/HighlightOff';
import TuneIcon from '@mui/icons-material/Tune';
import LocalPrintshopOutlinedIcon from '@mui/icons-material/LocalPrintshopOutlined';
import IosShareOutlinedIcon from '@mui/icons-material/IosShareOutlined';
import { Filter } from '../components/Filter/Filter';
import { useApi } from '../hooks/useApi';
import { DeleteMenu, GetOneMenu, UpadteMenu } from '../api/menu.api';
import { RecieptFullData } from '../components/Product/RecieptFullData';
import { RecieptIgredientsItem } from '../components/Menu/RecieptIgredientsItem';
import { RecieptBigItem } from '../components/Product/RecieptBigItem';
import { useNavigate, useParams } from 'react-router-dom';
import { GetManyRecipes } from '../api/recipe.api';
import { ActionApprove } from '../components/Default/ActionApprove/ActionApprove';
import { useDispatch } from 'react-redux';
import { setAlert } from '../store/alert.store';
import { url } from '../utils/url';

const days = [
  { key: 'mon', name: 'Понедельник' },
  { key: 'tues', name: 'Вторник' },
  { key: 'wed', name: 'Среда' },
  { key: 'thur', name: 'Четверг' },
  { key: 'fri', name: 'Пятница' },
  { key: 'sat', name: 'Суббота' },
  { key: 'sun', name: 'Воскресенье' },
];

export const SingleMenuPage = () => {
  const { menuId } = useParams();

  const [items, setItems] = useState([]);

  const [openFilter, setOpenFilter] = useState(false);
  const [currentDay, setCurrentDay] = useState(null);
  const [approveDeleteMenu, setApproveDeleteMenu] = useState(false);
  const [selectedReciept, setSelectedReciept] = useState(null);
  const [openAddProductDrawer, setOpenAddProductDrawer] = useState(false);

  const [menu, setMenu] = useState({});

  const GetOneMenuApi = useApi(GetOneMenu);
  const DeleteMenuApi = useApi(DeleteMenu);

  const UpdateMenuApi = useApi(UpadteMenu);
  const UpdateMenuHandler = async (data) => {
    await UpdateMenuApi.sendRequest(data, menuId);
    await GetOneManuHandler();
  };
  const dispatch = useDispatch();
  const navigate = useNavigate();
  const DeleteMenuHandler = async () => {
    await DeleteMenuApi.sendRequest(menuId);
    setApproveDeleteMenu(false);
    dispatch(setAlert({ text: 'Меню успешно удалено', status: 200 }));
    navigate(url.menu);
  };

  const reloadMenuhandler = async () => {
    await GetOneManuHandler();
  };

  const GetOneManuHandler = async () => {
    let result = await GetOneMenuApi.sendRequest({ id: menuId });
    if (result.by_week_days && !result.structured_menu) {
      result.structured_menu = {
        mon: [],
        tues: [],
        wed: [],
        thur: [],
        fri: [],
        sat: [],
        sun: [],
      };
    }
    setMenu(result);
  };

  const [openRecieptIngredientsDrawer, setOpenRecieptIngredientsDrawer] =
    useState(false);

  const [recipes, setRecipes] = useState([]);
  const GetManyRecipesApi = useApi(GetManyRecipes);
  const [filter, setFilter] = useState();
  const [offset, setOffset] = useState(0);
  const [recipesTotalCount, setRecipesTotalCount] = useState(0);

  const GetManyRecipesHandler = async () => {
    const result = await GetManyRecipesApi.sendRequest({
      limit: 16,
      offset,
      filter,
      // sorting
    });

    setRecipes(result.data);
    setRecipesTotalCount(result.totalItems);
  };

  useEffect(() => {
    GetOneManuHandler();
  }, []);
  useEffect(() => {
    GetManyRecipesHandler();
  }, [offset]);
  const onColumnChange = (id, day) => {
    setItems((prevItems) =>
      prevItems.map((item) => {
        if (item.id === id) {
          item.day = day;
        }
        return item;
      }),
    );
  };

  const IngredientItem = ({ item }) => {
    return (
      <Box>
        <Typography
          as="p"
          variant="p"
          sx={{ fontWeight: 'bold', mb: 1, mt: 2 }}
        >
          {item.title}
        </Typography>
        {item?.ingredients?.map((ing, index) => (
          <>
            <Typography as="p" variant="p" sx={{ mb: 1 }}>
              <span
                style={{
                  fontWeight: 'bold',
                  color: '#818181',
                  margin: '0 10px',
                }}
              >
                {index + 1}
              </span>{' '}
              {ing?.label?.title} {ing?.count} {ing?.measure}
            </Typography>
          </>
        ))}
      </Box>
    );
  };
  return (
    <MainLayout>
      <Grid container spacing={2}>
        <Grid item lg={12}>
          <Stack
            direction="row"
            spacing={2}
            alignItems="center"
            sx={{
              mb: 5,
            }}
          >
            <input
              style={{
                border: 'none',
                outline: 'none',
                fontSize: '30px',
                fontWeight: 'bold',
                width: '100%',
              }}
              placeholder="Название вашего меню"
              value={menu?.title}
              onChange={({ target: { value } }) =>
                setMenu({
                  ...menu,
                  title: value,
                })
              }
              onBlur={() => UpdateMenuHandler({ title: menu?.title })}
            />
            <Stack direction="row" spacing={1} alignItems="center">
              <Box sx={{ width: '150px' }}>
                <Button
                  onClick={() => setApproveDeleteMenu(true)}
                  color="error"
                  variant="outlined"
                  sx={{ fontSize: '12px' }}
                  fullWidth
                >
                  Удалить меню
                </Button>
              </Box>
              <Box sx={{ minWidth: '150px' }}>
                <Button
                  onClick={() => setOpenRecieptIngredientsDrawer(true)}
                  color="inherit"
                  variant="outlined"
                  sx={{ fontSize: '12px', opacity: 0.4 }}
                  fullWidth
                >
                  Список покупок
                </Button>
              </Box>
              <Box>
                <IconButton
                  color="inherit"
                  aria-label="Remove product from day"
                  // onClick={() => addProductHandler(id)}

                  sx={{ fontSize: '8px', opacity: 0.4 }}
                >
                  <IosShareOutlinedIcon />
                </IconButton>
              </Box>
              <Box>
                <IconButton
                  color="inherit"
                  aria-label="Remove product from day"
                  // onClick={() => addProductHandler(id)}

                  sx={{ fontSize: '8px', opacity: 0.4 }}
                >
                  <LocalPrintshopOutlinedIcon />
                </IconButton>
              </Box>
            </Stack>
          </Stack>
          {!menu?.by_week_days ? (
            <Grid container spacing={2}>
              <Grid item xs={12}>
                <Button
                  color="success"
                  variant="outlined"
                  aria-label="add new product to day"
                  onClick={() => setOpenAddProductDrawer(true)}
                  size="small"
                  // startIcon={<AddCircleOutlineOutlinedIcon />}
                >
                  Добавить
                </Button>
              </Grid>
              {menu?.unstructured_menu?.map((recipe) => (
                <Grid item xs={3}>
                  <RecieptBigItem
                    reciept={{ recipe }}
                    key={recipe?._id}
                    num_serv={recipe?.num_serv || 0}
                    fullMenu={menu}
                    currentDay={currentDay}
                    reloadMenuhandler={reloadMenuhandler}
                    updateFromMenu={true}
                    onRemoveItem={(recipe) => {
                      if (menu.by_week_days) {
                        UpdateMenuHandler({
                          structured_menu: {
                            ...menu.structured_menu,
                            [currentDay]: [
                              ...menu.structured_menu[currentDay].filter(
                                (item) => item._id !== recipe._id,
                              ),
                            ],
                          },
                        });
                      } else {
                        UpdateMenuHandler({
                          unstructured_menu: [
                            ...menu?.unstructured_menu?.filter(
                              (item) => item._id !== recipe._id,
                            ),
                          ],
                        });
                      }
                    }}
                    showRemoveItem={true}
                    onClickToReciept={(reciept) => setSelectedReciept(reciept)}
                  />
                </Grid>
              ))}
            </Grid>
          ) : (
            <>
              <DndProvider backend={HTML5Backend}>
                <Stack sx={{ width: '100%' }}>
                  {days.map((day) => {
                    return (
                      <>
                        <Column
                          key={day}
                          onColumnChange={onColumnChange}
                          items={
                            menu?.structured_menu
                              ? menu?.structured_menu[day.key]
                              : []
                          }
                          fullMenu={menu}
                          title={day.name}
                          id={day.key}
                          currentDay={day.key}
                          onNewItem={(product) => {
                            UpdateMenuHandler({
                              structured_menu: {
                                ...(menu?.structured_menu
                                  ? menu?.structured_menu
                                  : []),
                                [day.key]: [
                                  ...(menu?.structured_menu
                                    ? menu?.structured_menu[day.key]
                                    : {}),
                                  product,
                                ],
                              },
                            });
                          }}
                          onRemoveItem={(recipe) => {
                            if (menu.by_week_days) {
                              console.log(day.key, [
                                ...(menu?.structured_menu[currentDay]?.filter(
                                  (item) => item._id !== recipe._id,
                                ) || []),
                              ]);

                              UpdateMenuHandler({
                                structured_menu: {
                                  ...menu.structured_menu,
                                  [day.key]: [
                                    ...(menu?.structured_menu[day.key]?.filter(
                                      (item) => item._id !== recipe._id,
                                    ) || []),
                                  ],
                                },
                              });
                            } else {
                              UpdateMenuHandler({
                                unstructured_menu: [
                                  ...menu.unstructured_menu.filter(
                                    (item) => item._id !== recipe._id,
                                  ),
                                ],
                              });
                            }
                          }}
                          reloadMenuhandler={reloadMenuhandler}
                          addProductHandler={(data) => {
                            setOpenAddProductDrawer(true);

                            setCurrentDay(data);
                          }}
                          selecteRecieptHandler={(reciept) => {
                            setSelectedReciept(reciept);
                            setCurrentDay(day.key);
                          }}
                        ></Column>
                      </>
                    );
                  })}
                </Stack>
              </DndProvider>
            </>
          )}

          <Drawer
            anchor="right"
            sx={{ height: '100vh' }}
            open={openAddProductDrawer}
            onClose={() => setOpenAddProductDrawer(false)}
          >
            <Stack direction="row" sx={{ height: '100%', maxWidth: '90%' }}>
              {openFilter && (
                <Filter
                  setFilter={(data) => setFilter(data)}
                  filter={filter}
                  filterButtonHandler={GetManyRecipesHandler}
                />
              )}
              <Box sx={{ height: '100vh', overflow: 'scroll' }}>
                <Stack
                  direction="row"
                  spacing={2}
                  justifyContent="space-between"
                  alignItems="center"
                >
                  <Box>
                    <Typography as="p" variant="h6" sx={{ p: 2 }}>
                      Выберите блюдо
                    </Typography>
                  </Box>
                  <Stack direction="row" spacing={1}>
                    <Box>
                      <IconButton
                        color="inherit"
                        aria-label="Remove product from day"
                        onClick={() => setOpenFilter(!openFilter)}
                        sx={{ fontSize: '8px', opacity: 0.4 }}
                      >
                        <TuneIcon />
                      </IconButton>
                    </Box>
                    <Box>
                      <IconButton
                        color="inherit"
                        aria-label="Remove product from day"
                        onClick={() => setOpenAddProductDrawer(false)}
                        sx={{ fontSize: '8px', opacity: 0.4, mr: 1 }}
                      >
                        <HighlightOffIcon />
                      </IconButton>
                    </Box>
                  </Stack>
                </Stack>
                <Box sx={{ px: 3, maxWidth: '100%', width: '650px' }}>
                  <Grid container spacing={2}>
                    {recipes?.map((recipe) => (
                      <Grid item xs={6}>
                        <RecieptBigItem
                          reciept={{ recipe }}
                          key={recipe?._id}
                          fullMenu={menu}
                          showCounts={false}
                          currentDay={currentDay}
                          num_serv={() => {
                            let result = 1;
                            if (menu.by_week_days) {
                              for (const i in menu?.structured_menu) {
                                const candidate = menu?.structured_menu[i].find(
                                  (item) => item._id == recipe._id,
                                );
                                if (candidate) {
                                  result = candidate.num_serv;
                                }
                              }
                            } else {
                              const candidate = menu?.unstructured_menu?.find(
                                (item) => item._id == recipe._id,
                              );
                              if (candidate) {
                                result = candidate.num_serv;
                              }
                            }
                            return result;
                          }}
                          updateFromMenu={true}
                          onRemoveItem={() => {
                            if (menu.by_week_days) {
                              UpdateMenuHandler({
                                structured_menu: {
                                  ...menu.structured_menu,
                                  [currentDay]: [
                                    ...menu.structured_menu[currentDay].filter(
                                      (item) => item._id !== recipe._id,
                                    ),
                                  ],
                                },
                              });
                            } else {
                              UpdateMenuHandler({
                                unstructured_menu: [
                                  ...menu.unstructured_menu.filter(
                                    (item) => item._id !== recipe._id,
                                  ),
                                ],
                              });
                            }
                          }}
                          onClickToReciept={(item) => setSelectedReciept(item)}
                          addToMenuHandler={(data) => {
                            if (menu.by_week_days) {
                              UpdateMenuHandler({
                                structured_menu: {
                                  ...menu?.structured_menu,
                                  [currentDay]: [
                                    ...(menu?.structured_menu[currentDay]
                                      ? menu?.structured_menu[currentDay]
                                      : []),
                                    recipe,
                                  ],
                                },
                              });
                            } else {
                              UpdateMenuHandler({
                                unstructured_menu: [
                                  ...(menu?.unstructured_menu || []),
                                  recipe,
                                ],
                              });
                            }
                            setOpenAddProductDrawer(false);
                          }}
                        />
                      </Grid>
                    ))}
                  </Grid>
                </Box>
              </Box>
            </Stack>
          </Drawer>

          <Drawer
            anchor="right"
            open={selectedReciept}
            sx={{ maxHeight: '100vh' }}
            onClose={() => setSelectedReciept(null)}
          >
            <RecieptFullData
              reciept={selectedReciept}
              addToMenuHandler={(reciept) => {
                setItems([
                  ...items,
                  { ...reciept, id: uuid(), day: currentDay },
                ]);
                setSelectedReciept(null);
                setOpenAddProductDrawer(false);
              }}
              fullmenu={menu}
              showCounts={false}
              currentDay={currentDay}
              removeToMenuHandler={(reciept) => {
                setItems(items?.filter((i) => i.id !== reciept.id));
                setSelectedReciept(null);
                setOpenAddProductDrawer(false);
              }}
            />
          </Drawer>

          <Drawer
            anchor="right"
            open={openRecieptIngredientsDrawer}
            sx={{ maxHeight: '100vh' }}
            onClose={() => setOpenRecieptIngredientsDrawer(false)}
          >
            <Box sx={{ p: 3, maxWidth: '500px' }}>
              <Stack direction="row" spacing={1} justifyContent="space-between">
                <Box>
                  <Typography as="h3" variant="h6">
                    Список покупок по меню
                  </Typography>
                </Box>
                <Box>
                  <IconButton
                    color="inherit"
                    aria-label="Remove product from day"
                    // onClick={() => addProductHandler(id)}

                    sx={{ fontSize: '8px', opacity: 0.4 }}
                  >
                    <LocalPrintshopOutlinedIcon />
                  </IconButton>
                </Box>
              </Stack>
              {!menu?.by_week_days ? (
                menu?.unstructured_menu?.map((item) => (
                  <IngredientItem item={item} />
                ))
              ) : (
                <>
                  <Typography
                    as="p"
                    variant="p"
                    sx={{ fontWeight: 'bold', mb: 1, mt: 2 }}
                  >
                    Понедельник
                  </Typography>
                  {menu?.structured_menu['mon']?.length
                    ? menu?.structured_menu['mon']?.map((item) => (
                        <IngredientItem item={item} />
                      ))
                    : 'Нет добавленных рецептов'}

                  <Typography
                    as="p"
                    variant="p"
                    sx={{ fontWeight: 'bold', mb: 1, mt: 2 }}
                  >
                    Вторник
                  </Typography>
                  {menu?.structured_menu['tues']?.length
                    ? menu?.structured_menu['tues']?.map((item) => (
                        <IngredientItem item={item} />
                      ))
                    : 'Нет добавленных рецептов'}

                  <Typography
                    as="p"
                    variant="p"
                    sx={{ fontWeight: 'bold', mb: 1, mt: 2 }}
                  >
                    Среда
                  </Typography>
                  {menu?.structured_menu['wed']?.length
                    ? menu?.structured_menu['wed']?.map((item) => (
                        <IngredientItem item={item} />
                      ))
                    : 'Нет добавленных рецептов'}

                  <Typography
                    as="p"
                    variant="p"
                    sx={{ fontWeight: 'bold', mb: 1, mt: 2 }}
                  >
                    Четверг
                  </Typography>
                  {menu?.structured_menu['thur']?.length
                    ? menu?.structured_menu['thur']?.map((item) => (
                        <IngredientItem item={item} />
                      ))
                    : 'Нет добавленных рецептов'}
                  <Typography
                    as="p"
                    variant="p"
                    sx={{ fontWeight: 'bold', mb: 1, mt: 2 }}
                  >
                    Пятница
                  </Typography>
                  {!!menu?.structured_menu['fri']?.length
                    ? menu?.structured_menu['fri']?.map((item) => (
                        <IngredientItem item={item} />
                      ))
                    : 'Нет добавленных рецептов'}

                  <Typography
                    as="p"
                    variant="p"
                    sx={{ fontWeight: 'bold', mb: 1, mt: 2 }}
                  >
                    Суббота
                  </Typography>
                  {!!menu?.structured_menu['sat']?.length
                    ? menu?.structured_menu['sat']?.map((item) => (
                        <IngredientItem item={item} />
                      ))
                    : 'Нет добавленных рецептов'}

                  <Typography
                    as="p"
                    variant="p"
                    sx={{ fontWeight: 'bold', mb: 1, mt: 2 }}
                  >
                    Воскресенье
                  </Typography>
                  {!!menu?.structured_menu['sun']?.length
                    ? menu?.structured_menu['sun']?.map((item) => (
                        <IngredientItem item={item} />
                      ))
                    : 'Нет добавленных рецептов'}
                </>
              )}
            </Box>
          </Drawer>
        </Grid>
      </Grid>
      <ActionApprove
        title="Удалить меню?"
        description="Вы действительно хотите удалить меню? При удалении все данные будут безвозвратно потеряны."
        onClose={() => setApproveDeleteMenu(false)}
        onSuccess={DeleteMenuHandler}
        open={approveDeleteMenu}
      />
    </MainLayout>
  );
};
