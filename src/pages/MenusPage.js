import React, { useEffect, useState } from 'react';
import { MainLayout } from '../components/Default/Layout/MainLayout';
import { MenuItem } from '../components/Menu/MenuItem';
import { Grid, Pagination, Typography } from '@mui/material';
import { HelloModal } from '../components/Modal/HelloModal';
import { InterfaceInstructionItem } from '../components/InterfaceInstruction/InterfaceInstructionItem';
import { setInstruction } from '../store/instruction.store';
import { useDispatch } from 'react-redux';
import { useCookies } from 'react-cookie';
import { GetManyMenu } from '../api/menu.api';
import { useApi } from '../hooks/useApi';
import AuthService from '../api/keycloak.api';

export const MenusPage = () => {
  const [cookie] = useCookies();
  const dispatch = useDispatch();
  const [offset, setOffset] = useState(0);
  const [totalItems, setTotalItems] = useState(0);
  const [menus, setManus] = useState([]);
  const GetManyMenuApi = useApi(GetManyMenu);
  const [page, setPage] = React.useState(1);
  const [user, setUser] = useState({});

  const handleChange = (event, value) => {
    setPage(value);
    setOffset((value - 1) * 16);
  };

  const GetManyMenuHandler = async () => {
    const result = await GetManyMenuApi.sendRequest({
      limit: 16,
      offset,
    });
    setManus(result.data);
    setTotalItems(result.totalItems);
  };

  useEffect(() => {
    GetManyMenuHandler();
  }, [offset]);

  useEffect(() => {
    AuthService.getUserInfo().then((result) => setUser(result));
  }, []);
  return (
    <MainLayout>
      <Grid container spacing={2}>
        <Grid item xs={12}>
          <Typography as="h1" variant="h4" sx={{ fontWeight: 'bold', mb: 5 }}>
            Привет, {user.given_name || user.email}! 👋
          </Typography>
        </Grid>
      </Grid>
      <Grid container spacing={2} sx={{ mb: 3 }}>
        {menus?.map((menu, index) => (
          <Grid item lg={3} sx={{ mb: 1 }}>
            <MenuItem id={index == 0 && `ViewDemoMenu`} menu={menu} />
          </Grid>
        ))}
      </Grid>
      {Math.ceil(totalItems / 16) > 1 && (
        <Grid container spacing={2}>
          <Grid item xs={12}>
            <Pagination
              sx={{ mb: 3 }}
              count={Math.ceil(totalItems / 16)}
              page={page}
              onChange={handleChange}
            />
          </Grid>
        </Grid>
      )}

      {cookie.instruction === true && <HelloModal />}
      <InterfaceInstructionItem
        step="ViewDemoMenu"
        title="Или посмотрите меню, которые мы создали для вас!"
        actionHandler={() =>
          dispatch(setInstruction({ step: 'ViewInstructionAlways' }))
        }
        actionText="Продолжить"
      />
    </MainLayout>
  );
};
