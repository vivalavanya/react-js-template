export const BACKEND_URL = 'https://meal-me.ru/backend';
export const url = {
  home: '/',
  menu: '/menu',
  reciepts: '/reciepts',
  singleMenu: '/menu/:menuId',
  addMenu: '/menu/create',
  wishlist: '/wishlist',
  login: '/login',
  registration: '/registration',
};
