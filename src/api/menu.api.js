import axios from 'axios';
import { BACKEND_URL } from '../utils/url';
import AuthService from './keycloak.api';

export const GetManyMenu = async ({ limit = 16, offset = 0 }) => {
  try {
    const token = AuthService.getToken();
    const { data } = await axios.get(`${BACKEND_URL}/menu/many`, {
      params: {
        limit,
        offset,
      },
      headers: {
        Authorization: `Bearer ${token}`,
        'Content-Type': 'application/json',
      },
    });
    return data;
  } catch (error) {
    throw error;
  }
};

export const GetOneMenu = async ({ id }) => {
  try {
    const token = AuthService.getToken();
    const { data } = await axios.get(`${BACKEND_URL}/menu/one`, {
      params: {
        id,
      },
      headers: {
        Authorization: `Bearer ${token}`,
        'Content-Type': 'application/json',
      },
    });
    return data;
  } catch (error) {
    throw error;
  }
};

export const CreateMenu = async ({ by_week_days = false, title = '' }) => {
  try {
    const token = AuthService.getToken();
    const { data } = await axios.post(
      `${BACKEND_URL}/menu`,
      {
        by_week_days,
        title,
      },
      {
        headers: {
          Authorization: `Bearer ${token}`,
          'Content-Type': 'application/json',
        },
      },
    );
    return data;
  } catch (error) {
    throw error;
  }
};

export const UpadteMenu = async (body, id) => {
  try {
    const token = AuthService.getToken();
    const { data } = await axios.put(`${BACKEND_URL}/menu`, body, {
      params: { id },
      headers: {
        Authorization: `Bearer ${token}`,
        'Content-Type': 'application/json',
      },
    });
    return data;
  } catch (error) {
    throw error;
  }
};

export const DeleteMenu = async (id) => {
  try {
    const token = AuthService.getToken();
    const { data } = await axios.delete(`${BACKEND_URL}/menu`, {
      params: { id },
      headers: {
        Authorization: `Bearer ${token}`,
        'Content-Type': 'application/json',
      },
    });
    return data;
  } catch (error) {
    throw error;
  }
};
