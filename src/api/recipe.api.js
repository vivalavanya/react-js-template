import axios from 'axios';
import { BACKEND_URL } from '../utils/url';
import AuthService from './keycloak.api';

export const GetIngredients = async ({ prefix = '' }) => {
  try {
    const token = AuthService.getToken();
    const { data } = await axios.get(`${BACKEND_URL}/recipe/find_ingredients`, {
      params: {
        prefix,
      },
      headers: {
        Authorization: `Bearer ${token}`,
        'Content-Type': 'application/json',
      },
    });
    return data;
  } catch (error) {
    throw error;
  }
};
export const GetManyRecipes = async ({
  limit = 16,
  offset = 0,
  filter = {},
  sorting = {},
}) => {
  try {
    console.log({ filter });

    const token = AuthService.getToken();
    const { data } = await axios.post(
      `${BACKEND_URL}/recipe/find_recipes`,
      { filter },
      {
        params: {
          limit,
          offset,
        },
        headers: {
          Authorization: `Bearer ${token}`,
          'Content-Type': 'application/json',
        },
      },
    );
    return data;
  } catch (error) {
    throw error;
  }
};

export const UpvoteeRecipe = async ({ id }) => {
  try {
    const token = AuthService.getToken();
    const { data } = await axios.put(
      `${BACKEND_URL}/recipe/upvote`,

      {},
      {
        headers: {
          Authorization: `Bearer ${token}`,
          'Content-Type': 'application/json',
        },
        params: {
          id,
        },
      },
    );
    return data;
  } catch (error) {
    throw error;
  }
};
export const DownvoteeRecipe = async ({ id }) => {
  try {
    const token = AuthService.getToken();
    const { data } = await axios.put(
      `${BACKEND_URL}/recipe/downvote`,
      {},
      {
        params: {
          id,
        },
        headers: {
          Authorization: `Bearer ${token}`,
          'Content-Type': 'application/json',
        },
      },
    );
    return data;
  } catch (error) {
    throw error;
  }
};

export const FavoritesRecipe = async () => {
  try {
    const token = AuthService.getToken();
    const { data } = await axios.get(
      `${BACKEND_URL}/recipe/favorites`,

      {
        headers: {
          Authorization: `Bearer ${token}`,
          'Content-Type': 'application/json',
        },
      },
    );
    return data;
  } catch (error) {
    throw error;
  }
};
