import axios from 'axios';
import { BACKEND_URL } from '../utils/url';
import AuthService from './keycloak.api';

export const Login = async ({ password, username }) => {
  try {
    const { data } = await axios.post(`${BACKEND_URL}/menu`, {
      username,
      password,
    });
    return data;
  } catch (error) {
    throw error;
  }
};
